
#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

#include "ColorSpace_To_Grayscale_2.h"
#include "ColorSpace_To_Grayscale_parameters.h"

int main()
{
	int doColor_To_Grayscale(

		const Image& image_in,

		//const char *inputFileNamef,
		//const char *outputFileNamef,

		const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
		//const PARAMETERS_COLOR_TO_GRAYSCALE &sParameters_Color_To_Grayscalef,

		Image& image_out);

	int
		nRes;

	//const char inputFileName[] = "Orig_Red_Cyan.png";

	const char inputFileName[] = "01-203-0-RMLO_Neg_c_abX_c_TX.png";

	const char outputFileName[] = "ImageSaved_Gray.png";

	PARAMETERS_COLOR_TO_GRAYSCALE sParameters_Color_To_Grayscale;
	/////////////////////////////////////////////////////////////////////////////////////////////////

	sParameters_Color_To_Grayscale.bIS_HISTOGRAM_EQUALIZATION_INCLUDEDf = bIS_HISTOGRAM_EQUALIZATION_INCLUDED;

	sParameters_Color_To_Grayscale.fFadingFactorf = fFadingFactor;

	sParameters_Color_To_Grayscale.fWeight_Redf = fWeight_Red;

	sParameters_Color_To_Grayscale.fWeight_Greenf = fWeight_Green;

	sParameters_Color_To_Grayscale.fWeight_Bluef = fWeight_Blue;

	sParameters_Color_To_Grayscale.fWeight_Cyanf = fWeight_Cyan;

	sParameters_Color_To_Grayscale.fWeight_Magentaf = fWeight_Magenta;

	sParameters_Color_To_Grayscale.fWeight_Yellowf = fWeight_Yellow;


	sParameters_Color_To_Grayscale.fWeight_Lf = fWeight_L;

	sParameters_Color_To_Grayscale.fWeight_Af = fWeight_A;

	sParameters_Color_To_Grayscale.fWeight_Bf = fWeight_B;

	sParameters_Color_To_Grayscale.fWeight_Convert_RedToGrayscalef = fWeight_Convert_RedToGrayscale;

	sParameters_Color_To_Grayscale.fWeight_Convert_GreenToGrayscalef = fWeight_Convert_GreenToGrayscale;

	sParameters_Color_To_Grayscale.fWeight_Convert_BlueToGrayscalef = fWeight_Convert_BlueToGrayscale;

	sParameters_Color_To_Grayscale.bINCLUDE_EXPf = bINCLUDE_EXP;

	sParameters_Color_To_Grayscale.nThreshPixelIntensityForExpf = nThreshPixelIntensityForExp;

	sParameters_Color_To_Grayscale.fConstForExpf = fConstForExp;

	sParameters_Color_To_Grayscale.bINCLUDE_PIXEL_INTENSITY_SUBSTITUTIONf = bINCLUDE_PIXEL_INTENSITY_SUBSTITUTION;

	sParameters_Color_To_Grayscale.nPixelIntensToBeSubstituted_Minf = nPixelIntensToBeSubstituted_Min;

	sParameters_Color_To_Grayscale.nPixelIntensToBeSubstituted_Maxf = nPixelIntensToBeSubstituted_Max;

	sParameters_Color_To_Grayscale.nPixelIntensToSubstitutef = nPixelIntensToSubstitute;

	sParameters_Color_To_Grayscale.bINCLUDE_RGB_FROM_WEIGHTED_HLS_BEFORE_PROCESSINGf = bINCLUDE_RGB_FROM_WEIGHTED_HLS_BEFORE_PROCESSING;

	sParameters_Color_To_Grayscale.bINCLUDE_RGB_FROM_WEIGHTED_HLS_AFTER_PROCESSINGf = bINCLUDE_RGB_FROM_WEIGHTED_HLS_AFTER_PROCESSING;

	sParameters_Color_To_Grayscale.fWeightOfHuef = fWeightOfHue;

	sParameters_Color_To_Grayscale.fWeightOfLightnessf = fWeightOfLightness;

	sParameters_Color_To_Grayscale.fWeightOfSaturationf = fWeightOfSaturation;

	sParameters_Color_To_Grayscale.nHueMinf = nHueMin;

	sParameters_Color_To_Grayscale.nHueMaxf = nHueMax;

	sParameters_Color_To_Grayscale.bSELECTING_COLOR_RANGES_AT_THE_READINGf = bSELECTING_COLOR_RANGES_AT_THE_READING;

	sParameters_Color_To_Grayscale.bSELECTING_COLOR_RANGES_AT_THE_HLS_OUTPUTf = bSELECTING_COLOR_RANGES_AT_THE_HLS_OUTPUT;

	sParameters_Color_To_Grayscale.nRedMinf = nRedMin;
	sParameters_Color_To_Grayscale.nRedMaxf = nRedMax;
	///////////////////////////////////////////////////////////
	sParameters_Color_To_Grayscale.nGreenMinf = nGreenMin;
	sParameters_Color_To_Grayscale.nGreenMaxf = nGreenMax;

	sParameters_Color_To_Grayscale.nBlueMinf = nBlueMin;
	sParameters_Color_To_Grayscale.nBlueMaxf = nBlueMax;

	////////////////////////////////////////////////////////////////////////////////////////////////

	Image image_in;
	//image_in.read("Orig_Red_Cyan.png");

	//	bool status = image_in.read("IM - 0001 - 0738_CI.png");
	image_in.read(inputFileName);

	Image image_out;

	nRes = doColor_To_Grayscale(
		//inputFileName, //const char *inputFileNamef,
		image_in, // Image& image_in,

				  //outputFileName,	//const char *outputFileNamef, 

		&sParameters_Color_To_Grayscale, // const PARAMETERS_WEIGHTED_GRAY_3x3 *sParameters_Color_To_Grayscalef); // const float fWeight_Bluef)
										 //sParameters_Color_To_Grayscale, // const PARAMETERS_WEIGHTED_GRAY_3x3 *sParameters_Color_To_Grayscalef); // const float fWeight_Bluef)

		image_out); // Image& image_out);

	image_out.write("Image_Output_Gray.png");

	printf("\n\n The grayscale image has been saved");
	printf("\n\n Please press any key to exit");
	getchar(); //exit(1);
	return 1;

} //int main()

  //printf("\n\n Please press any key:"); getchar();
