#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

//using namespace imago;

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
//#include <iostream.h>
#include <string.h>
//#include <iomanip.h>
//#include <fstream.h>
#include <assert.h>

#include "image.h"
using namespace imago;

#define SQRT2 1.414213562
#define BLACK 0
#define WHITE 1

#define nLenMax 6000 
#define nWidMax 6000 

#define nLenMin 50 
#define nWidMin 50

#define nImageSizeMax (nLenMax*nWidMax)

///////////////////////////////////////////////////////////////////
#define nLarge 1000000
#define fLarge 1.0E+10 //6

#define feps 0.000001
//#define pi 3.14159
#define PI 3.1415926535

//////////////////////////////////////////////////////////////////
/*
#define nLenSubimageToPrintMin 900 //342 
#define nLenSubimageToPrintMax 1300 //426

#define nWidSubimageToPrintMin 350 //814 
#define nWidSubimageToPrintMax 550
*/
///////////////////////////////////////////////////////////////////////

#define nIntensityStatMin 10 //85 
#define nIntensityStatMinForEqualization 0 //85 

#define nIntensityStatForReadMax 65535

#define nIntensityStatMax 255 

//#define nNumOfHistogramBinsStat (nIntensityStatMax) //(nIntensityStatMax - nIntensityStatMin + 1)
#define nNumOfHistogramBinsStat (nIntensityStatMax + 1) //(nIntensityStatMax - nIntensityStatMin + 1)

#define nIntensityThresholdForDense 150  // > nIntensityStatMin) and < nIntensityStatMax

#define fHueUndefinedValue (-1.0)

/////////////////////////////////////////////////////////

typedef struct
{
	int
		nRed;

	int
		nGreen;
	int
		nBlue;

} WEIGHTED_RGB_COLORS;

typedef struct
{
	int
		nCyan;
	int
		nMagenta;
	int
		nYellow;
	int
		nBlack;

	float
		fWeightCyan;
	float
		fWeightMagenta;
	float
		fWeightYellow;

	int
		nRed_FrWeightedCyan;

	int
		nGreen_FrWeightedMagenta;

	int
		nBlue_FrWeightedYellow;

} WEIGHTED_CMYK_COLORS;

typedef struct
{
	//no weighting for XYZ
	float
		fX;
	float
		fY;
	float
		fZ;

} XYZ_COLORS;


typedef struct
{

	float fL_FrXYZf;
	float fA_FrXYZf;
	float fB_FrXYZf;

	float
		fWeighted_L;
	float
		fWeighted_A;
	float
		fWeighted_B;

} WEIGHTED_LAB_COLORS;

typedef struct
{
	int nWidth;
	int nLength;

	//	int nRed_Arr[nImageSizeMax];
	//	int nGreen_Arr[nImageSizeMax];
	//	int nBlue_Arr[nImageSizeMax];
	int *nRed_Arr;
	int *nGreen_Arr;
	int *nBlue_Arr;

} COLOR_IMAGE;


typedef struct
{
	int nWidth;
	int nLength;

	float *fHue_Arr;
	float *fLightness_Arr;
	float *fSaturation_Arr;

} HLS_IMAGE;

typedef struct
{
	int nWidth;
	int nLength;

	int *nPixel_ValidOrNotArr; //1 - valid, 0 - invalid
	int *nGrayScale_Arr;

} GRAYSCALE_IMAGE;

typedef struct
{
	bool bIS_HISTOGRAM_EQUALIZATION_INCLUDEDf; // = false;


	float fFadingFactorf; // = 1.0;

						  ////////////////////////////////////////////////////////////////////////
						  // The weight of Red (percents)

	float fWeight_Redf; // = 100.0;

						// The weight of Green (percents)
	float fWeight_Greenf; // = 100.0;

						  // The weight of Blue (percents)
	float fWeight_Bluef; // = 100.0;

						 //////////////////////////////////////////////////////////////////

						 // The weight of Cyan (percents)
	float fWeight_Cyanf; // = 0.0;

						 // The weight of Magenta (percents)
	float fWeight_Magentaf; // = 0.0;

							// The weight of Yellow (percents)
	float fWeight_Yellowf; // = 0.0;
						   //////////////////////////////////////////////////////////////////
						   // The weights for Lab

						   // The weight of L (percents)
	float fWeight_Lf; // = 0.0;

					  // The weight of A (percents)
	float fWeight_Af; // = 0.0;

					  // The weight of B (percents)
	float fWeight_Bf; // = 0.0;

					  ////////////////////////////////////////////////

					  //#define fWeight_Convert_RedToGrayscale (100.0/3) //
					  //#define fWeight_Red 4.0 // 

	float fWeight_Convert_RedToGrayscalef; // = (100.0 / 3);

										   // The weight of Green (percents)
										   //#define fWeight_Convert_GreenToGrayscale 48.0 //80.0 //40.0 //20.0 //(100.0/2.0) 

	float fWeight_Convert_GreenToGrayscalef; // = (100.0 / 3);

											 // The weight of Blue (percents)

											 //#define fWeight_Convert_BlueToGrayscale  (100.0/3) //(100.0/2.0) 
											 //#define fWeight_Convert_BlueToGrayscale 48.0 //20.0 //40.0 //(100.0/2.0) 

	float fWeight_Convert_BlueToGrayscalef; // = (100.0 / 3);

											/////////////////////////////////////////////////////////////////
											//#define INCLUDE_EXP

	bool bINCLUDE_EXPf; // = false;

	int nThreshPixelIntensityForExpf; // = 20; //50  //30 //10 //70 
	float fConstForExpf; // = 0.005; //0.01 //0.02 //0.08 //0.04 

						 //////////////////////////////////////////////////////////////////
						 //#define INCLUDE_PIXEL_INTENSITY_SUBSTITUTION
	bool bINCLUDE_PIXEL_INTENSITY_SUBSTITUTIONf; // = false;

	int nPixelIntensToBeSubstituted_Minf; // = 60; //80 //20 //40 

	int nPixelIntensToBeSubstituted_Maxf; // = 120; //80 //

	int nPixelIntensToSubstitutef; // = 180; //160

								   //////////////////////////////////////////////////////
								   //	 bINCLUDE_RGB_FROM_WIGHTED_HLS_BEFORE_PROCESSINGf
	bool bINCLUDE_RGB_FROM_WEIGHTED_HLS_BEFORE_PROCESSINGf; // = false;

	bool bINCLUDE_RGB_FROM_WEIGHTED_HLS_AFTER_PROCESSINGf; // = false;

	float fWeightOfHuef; // = 100.0;
	float fWeightOfLightnessf; // = 100.0;
	float fWeightOfSaturationf; // = 100.0; //0.0 

								//#define nHueMin 0 //0 //10
								//#define nHueMax 60 //240 //80 //360 //80
	int nHueMinf; // = 300; //340 
	int nHueMaxf; // = 360; //240 //80 //360 //80

	bool bSELECTING_COLOR_RANGES_AT_THE_READINGf; // = false; //true;

	bool bSELECTING_COLOR_RANGES_AT_THE_HLS_OUTPUTf; // = true;

													 //#define nRedMin 0 //0 //10
													 //#define nRedMax 255 //
	int nRedMinf; // = 140; //0 //10
	int nRedMaxf; // = 180; //

				  //#define nGreenMin 0 //0 //10
				  //#define nGreenMax 255 //
	int nGreenMinf; // = 10; //0 //10
	int nGreenMaxf; // = 60; //

					//#define nBlueMin 0 //0 //10
					//#define nBlueMax 255 //
	int nBlueMinf; // = 30; //0 //10
	int nBlueMaxf; // = 80; //


} PARAMETERS_COLOR_TO_GRAYSCALE;

int doColor_To_Grayscale(
	const Image& image_in,

	const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
	//const PARAMETERS_COLOR_TO_GRAYSCALE &sParameters_Color_To_Grayscalef,

	Image& image_out);
