#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1
//////////////////////////////////////////////////////////////////

//bool bIS_HISTOGRAM_EQUALIZATION_INCLUDED = true;
bool bIS_HISTOGRAM_EQUALIZATION_INCLUDED = false;

float fFadingFactor = 1.0;

////////////////////////////////////////////////////////////////////////
// The weight of Red (percents)
float fWeight_Red = 100.0;

// The weight of Green (percents)
float fWeight_Green = 100.0;

// The weight of Blue (percents)
float fWeight_Blue = 0.0;

//////////////////////////////////////////////////////////////////

// The weight of Cyan (percents)
//float fWeight_Cyan = 0.0;
float fWeight_Cyan = 0.0;

// The weight of Magenta (percents)
//float fWeight_Magenta = 0.0;
float fWeight_Magenta = 0.0;

// The weight of Yellow (percents)
//float fWeight_Yellow = 0.0;
float fWeight_Yellow = 200.0;
//////////////////////////////////////////////////////////////////
// The weights for Lab

// The weight of L (percents)
//float fWeight_L = 0.0;
float fWeight_L = 00.0;

// The weight of A (percents)
//float fWeight_A = 0.0;
float fWeight_A = 0.0;

// The weight of B (percents)
//float fWeight_B = 0.0;
float fWeight_B = 0.0;

////////////////////////////////////////////////
float fWeight_Convert_RedToGrayscale = 60;

// The weight of Green (percents)
float fWeight_Convert_GreenToGrayscale = 40;

// The weight of Blue (percents)
float fWeight_Convert_BlueToGrayscale = 0;

/////////////////////////////////////////////////////////////////
//#define INCLUDE_EXP

bool bINCLUDE_EXP = false;

int nThreshPixelIntensityForExp = 20; //50  //30 //10 //70 
float fConstForExp = 0.005; //0.01 //0.02 //0.08 //0.04 

							//////////////////////////////////////////////////////////////////
bool bINCLUDE_PIXEL_INTENSITY_SUBSTITUTION = false;

int nPixelIntensToBeSubstituted_Min = 50; //80 //20 //40

int nPixelIntensToBeSubstituted_Max = 140; //80 //

int nPixelIntensToSubstitute = 50; //160

								   //////////////////////////////////////////////////////
bool bINCLUDE_RGB_FROM_WEIGHTED_HLS_BEFORE_PROCESSING = false;

bool bINCLUDE_RGB_FROM_WEIGHTED_HLS_AFTER_PROCESSING = false;

float fWeightOfHue = 50.0;
float fWeightOfLightness = 140.0;
float fWeightOfSaturation = 50.0; //0.0

int nHueMin = 300; //340 
int nHueMax = 360; //240 //80 //360 //80

bool bSELECTING_COLOR_RANGES_AT_THE_READING = false; //true;

bool bSELECTING_COLOR_RANGES_AT_THE_HLS_OUTPUT = false;

int nRedMin = 140; //0 //10
int nRedMax = 180; //

int nGreenMin = 10; //0 //10
int nGreenMax = 60; //

int nBlueMin = 30; //0 //10
int nBlueMax = 80; //



