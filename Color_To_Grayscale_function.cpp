

#include "ColorSpace_To_Grayscale_2.h"

using namespace imago;

float
fL_Min_Glob = fLarge,
fL_Max_Glob = -fLarge,

fA_Min_Glob = fLarge,
fA_Max_Glob = -fLarge,

fB_Min_Glob = fLarge,
fB_Max_Glob = -fLarge;

FILE *fout;

//The reference colors at D65 (p.102, D.Sundararajan)
double
dX_Referf = 0.950456,
dY_Referf = 1.0,
dZ_Referf = 1.088754;

int doColor_To_Grayscale(
	const Image& image_in,

	const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
	//const PARAMETERS_COLOR_TO_GRAYSCALE &sParameters_Color_To_Grayscalef,

	Image& image_out)
{

	int Initializing_Color_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		COLOR_IMAGE *sColor_Imagef);

	int Initializing_HLS_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		HLS_IMAGE *sHLS_Imagef);

	int Initializing_GRAYSCALE_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		GRAYSCALE_IMAGE *sGRAYSCALE_Imagef);

	int Converting_AColorImageToAGrayscale(

		const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
		//const PARAMETERS_COLOR_TO_GRAYSCALE &sParameters_Color_To_Grayscalef,

		COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]
		GRAYSCALE_IMAGE *sGrayscale_Imagef); //[nImageSizeMax]

	int RGB_Fr_Weighted_HLS(
		const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
		//const PARAMETERS_COLOR_TO_GRAYSCALE &sParameters_Color_To_Grayscalef,

		HLS_IMAGE *sHLS_Imagef,

		COLOR_IMAGE *sColor_Imagef);

	int HistogramEqualization(

		GRAYSCALE_IMAGE *sGrayscale_Image);

	int
		nRes;

	///////////////////////////////////////////////////////////////////////////////
	fout = fopen("wMain_MultiColorToGrayscale.txt", "w");

	if (fout == NULL)
	{
		printf("\n\n fout == NULL");
		getchar(); exit(1);
	} //if (fout == NULL)

	if (fabs(sParameters_Color_To_Grayscalef->fWeight_Convert_RedToGrayscalef + sParameters_Color_To_Grayscalef->fWeight_Convert_GreenToGrayscalef + sParameters_Color_To_Grayscalef->fWeight_Convert_BlueToGrayscalef - 100.0) > feps)
	{
		printf("\n\n An error: percents for conversion to the grayscale image must add up to 100 ");
		fprintf(fout, "\n\n An error: percents for conversion to the grayscale image must add up to 100 ");

		printf("\n\n Please reassign sParameters_Color_To_Grayscalef->fWeight_Convert_RedToGrayscale, sParameters_Color_To_Grayscalef->fWeight_Convert_GreenToGrayscalef and sParameters_Color_To_Grayscalef->fWeight_Convert_BlueToGrayscalef");
		fflush(fout); getchar(); exit(1);
	} //if (fabs(sParameters_Color_To_Grayscalef->fWeight_Convert_RedToGrayscalef+ sParameters_Color_To_Grayscalef->fWeight_Convert_GreenToGrayscalef + sParameters_Color_To_Grayscalef->fWeight_Convert_BlueToGrayscalef - 100.0) > feps)

	  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	int
		i,
		j,

		nSizeOfImage, // = nImageWidth*nImageHeight,

		nIndexOfPixelCur,

		iLen,
		iWid,

		nRed,
		nGreen,
		nBlue,

		nIntensity_Read_Test_ImageMax = -nLarge,

		nImageWidth,
		nImageHeight;

	int
		nDiffOfIntensityAndThreshf;

	float
		fExpOfDiff;

	////////////////////////////////////////////////////////////////////////

	// size of image
	nImageWidth = image_in.width();

	nImageWidth = image_in.width();
	nImageHeight = image_in.height();

	printf("\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);

	fprintf(fout, "\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);

	if (nImageWidth > nLenMax || nImageWidth < nLenMin)
	{
		fprintf(fout, "\n\n An error in reading the image width: nImageWidth = %d", nImageWidth);

		printf("\n\n An error in reading the image width: nImageWidth = %d", nImageWidth);
		printf("\n\n Please press any key to exit");
		fflush(fout);
		getchar(); exit(1);

	} // if (nImageWidth > nLenMax || nImageWidth < nLenMin)

	if (nImageHeight > nWidMax || nImageHeight < nWidMin)
	{
		printf("\n\n An error in reading the image height: nImageHeight = %d", nImageHeight);
		printf("\n\n Please press any key to exit");
		getchar(); exit(1);

	} // if (nImageHeight > nWidMax || nImageHeight < nWidMin)

	COLOR_IMAGE sColor_Image; //

	nRes = Initializing_Color_To_CurSize(
		nImageHeight, //const int nImageWidthf,
		nImageWidth, //const int nImageLengthf,

		&sColor_Image); // COLOR_IMAGE *sColor_Imagef);

	GRAYSCALE_IMAGE sGrayscale_Image;

	nRes = Initializing_GRAYSCALE_To_CurSize(
		nImageHeight, //const int nImageWidthf,
		nImageWidth, //const int nImageLengthf,

		&sGrayscale_Image); // GRAYSCALE_IMAGE *sGRAYSCALE_Imagef)

	nSizeOfImage = nImageWidth*nImageHeight;

	//finding 'nIntensity_Read_Test_ImageMax' to decide if the intensity rescaling to (0,255) is needed
	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j*nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)
			{
				printf("\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
				printf("\n\n Please press any key to exit");
				getchar(); exit(1);
			} // if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)

			if (nRed > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nRed;

			if (nGreen > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nGreen;

			if (nBlue > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nBlue;

			//fprintf(fout, "\n nPixelArr[%d] = %d, i = %d, j = %d", nIndexOfPixel, nPixelArr[nIndexOfPixel], i, j);
		} // for (int i = 0; i < nImageWidth; i++)

	}//for (int j = 0; j < nImageHeight; j++)

	printf("\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
	fprintf(fout, "\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
	////////////////////////////////////////////////////////////////////////////////////////////

	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j*nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			iLen = i;
			iWid = j;

			if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			{
				//rescaling

				sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed / 256;

				sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen / 256;
				sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue / 256;

				//////////////////////////////////////////////////////////////////////////////////////////////////////
		
			} //if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			else
			{
				// no rescaling


				sColor_Image.nRed_Arr[nIndexOfPixelCur] = (int)(sParameters_Color_To_Grayscalef->fFadingFactorf*float(nRed));

				if (sColor_Image.nRed_Arr[nIndexOfPixelCur] > nIntensityStatMax)
					sColor_Image.nRed_Arr[nIndexOfPixelCur] = nIntensityStatMax;

				sColor_Image.nGreen_Arr[nIndexOfPixelCur] = (int)(sParameters_Color_To_Grayscalef->fFadingFactorf*float(nGreen)); //;

				if (sColor_Image.nGreen_Arr[nIndexOfPixelCur] > nIntensityStatMax)
					sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nIntensityStatMax;

				sColor_Image.nBlue_Arr[nIndexOfPixelCur] = (int)(sParameters_Color_To_Grayscalef->fFadingFactorf*float(nBlue)); //;

				if (sColor_Image.nBlue_Arr[nIndexOfPixelCur] > nIntensityStatMax)
					sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nIntensityStatMax;

			} //else

			  //////////////////////////////////////////////////////////////////////////////////////////////////////
			if (sParameters_Color_To_Grayscalef->bSELECTING_COLOR_RANGES_AT_THE_READINGf == true)
			{
				if (sColor_Image.nRed_Arr[nIndexOfPixelCur] < sParameters_Color_To_Grayscalef->nRedMinf || sColor_Image.nRed_Arr[nIndexOfPixelCur] > sParameters_Color_To_Grayscalef->nRedMaxf)
				{
					sColor_Image.nRed_Arr[nIndexOfPixelCur] = nIntensityStatMax;
					sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nIntensityStatMax;
					sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nIntensityStatMax;
					continue;
				} // if (sColor_Image.nRed_Arr[nIndexOfPixelCur] < sParameters_Color_To_Grayscalef->nRedMinf || sColor_Image.nRed_Arr[nIndexOfPixelCur] > sParameters_Color_To_Grayscalef->nRedMaxf)

				if (sColor_Image.nGreen_Arr[nIndexOfPixelCur] < sParameters_Color_To_Grayscalef->nGreenMinf || sColor_Image.nGreen_Arr[nIndexOfPixelCur] > sParameters_Color_To_Grayscalef->nGreenMaxf)
				{
					sColor_Image.nRed_Arr[nIndexOfPixelCur] = nIntensityStatMax;
					sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nIntensityStatMax;
					sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nIntensityStatMax;
					continue;
				} // if (sColor_Image.nGreen_Arr[nIndexOfPixelCur] < sParameters_Color_To_Grayscalef->nGreenMinf || sColor_Image.nGreen_Arr[nIndexOfPixelCur] > sParameters_Color_To_Grayscalef->nGreenMaxf)

				if (sColor_Image.nBlue_Arr[nIndexOfPixelCur] < sParameters_Color_To_Grayscalef->nBlueMinf || sColor_Image.nBlue_Arr[nIndexOfPixelCur] > sParameters_Color_To_Grayscalef->nBlueMaxf)
				{
					sColor_Image.nRed_Arr[nIndexOfPixelCur] = nIntensityStatMax;
					sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nIntensityStatMax;
					sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nIntensityStatMax;

					continue;
				} // if (sColor_Image.nBlue_Arr[nIndexOfPixelCur] < sParameters_Color_To_Grayscalef->nBlueMinf || sColor_Image.nBlue_Arr[nIndexOfPixelCur] > sParameters_Color_To_Grayscalef->nBlueMaxf)

			} // if (sParameters_Color_To_Grayscalef->bSELECTING_COLOR_RANGES_AT_THE_READINGf == true)

			  //fprintf(fout, "\n nPixelArr[%d] = %d, i = %d, j = %d", nIndexOfPixel, nPixelArr[nIndexOfPixel], i, j);
		} // for (int i = 0; i < nImageWidth; i++)

	}//for (int j = 0; j < nImageHeight; j++)

	 //printf("\n\n 1: please press any key"); getchar();
	 //#ifdef INCLUDE_RGB_FROM_WIGHTED_HLS_BEFORE_PROCESSING

	HLS_IMAGE
		sHLS_Image_Before_Processing;

	nRes = Initializing_HLS_To_CurSize(
		nImageHeight, //const int nImageWidthf,
		nImageWidth, //const int nImageLengthf,
		&sHLS_Image_Before_Processing); // COLOR_IMAGE *sColor_Imagef)


	if (sParameters_Color_To_Grayscalef->bINCLUDE_RGB_FROM_WEIGHTED_HLS_BEFORE_PROCESSINGf == true)
	{
		nRes = RGB_Fr_Weighted_HLS(

			//sParameters_Color_To_Grayscalef, //const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
			sParameters_Color_To_Grayscalef, //const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,

			&sHLS_Image_Before_Processing, //HLS_IMAGE *sHLS_Imagef,

			&sColor_Image); // COLOR_IMAGE *sColor_Imagef);

	} // if (sParameters_Color_To_Grayscalef->bINCLUDE_RGB_FROM_WEIGHTED_HLS_BEFORE_PROCESSINGf == true)

	  //printf("\n\n 2: please press any key"); getchar();

	  ///////////////////////////////////////////////////////////////////////////

	nRes = Converting_AColorImageToAGrayscale(

		sParameters_Color_To_Grayscalef, //const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
		&sColor_Image, //COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]
		&sGrayscale_Image); // GRAYSCALE_IMAGE *sGrayscale_Imagef) //[nImageSizeMax]


							/////////////////////////////////////////////////////////////////////////////////
	if (sParameters_Color_To_Grayscalef->bIS_HISTOGRAM_EQUALIZATION_INCLUDEDf == true)
	{
		nRes = HistogramEqualization(

			&sGrayscale_Image); // GRAYSCALE_IMAGE *sGrayscale_Image)

								//printf("\n\n After 'HistogramEqualization': please press any key to continue");
								//fflush(fout); getchar();

	} // if (sParameters_Color_To_Grayscalef->bIS_HISTOGRAM_EQUALIZATION_INCLUDEDf == true)

	  //printf("\n\n 3: please press any key"); getchar();

	  /////////////////////////////////////////////////////////////////////////////////////////////

	int bytesOfWidth = image_in.pitchInBytes();

	int nStep = image_in.pitchInBytes() / (image_in.width() * sizeof(unsigned char));

	// Save to file
	Image imageToSave(nImageWidth, nImageHeight, bytesOfWidth);

	for (j = 0; j < imageToSave.height(); j++)
	{
		for (i = 0; i < imageToSave.width(); i++)
		{
			iLen = i;
			iWid = j;

			nIndexOfPixelCur = iLen + iWid*nImageWidth;
			//nIndexOfPixelMax = iLen + (iWid*nLenMax);

			//#ifdef INCLUDE_EXP
			//adjusting 'sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur]'

			if (sParameters_Color_To_Grayscalef->bINCLUDE_EXPf == true)
			{
				nDiffOfIntensityAndThreshf = sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur] - sParameters_Color_To_Grayscalef->nThreshPixelIntensityForExpf;

				fExpOfDiff = exp((float)(nDiffOfIntensityAndThreshf)*sParameters_Color_To_Grayscalef->fConstForExpf);

				sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur] = (int)((float)(sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur])*fExpOfDiff);

				if (sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur] > nIntensityStatMax)
					sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur] = nIntensityStatMax;

			} // if (sParameters_Color_To_Grayscalef->bINCLUDE_EXPf == true)

			  //#ifdef INCLUDE_PIXEL_INTENSITY_SUBSTITUTION
			if (sParameters_Color_To_Grayscalef->bINCLUDE_PIXEL_INTENSITY_SUBSTITUTIONf == true)
			{
				if (sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur] > sParameters_Color_To_Grayscalef->nPixelIntensToBeSubstituted_Minf && sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur] < sParameters_Color_To_Grayscalef->nPixelIntensToBeSubstituted_Maxf)
					sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur] = sParameters_Color_To_Grayscalef->nPixelIntensToSubstitutef;
			} // if (sParameters_Color_To_Grayscalef->bINCLUDE_PIXEL_INTENSITY_SUBSTITUTIONf == true)


			imageToSave(j, i, R) = imageToSave(j, i, G) = imageToSave(j, i, B) = sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur];
			//image_out(j, i, R) = image_out(j, i, G) = image_out(j, i, B) = sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur];

			imageToSave(j, i, A) = image_in(j, i, A);
			//image_out(j, i, A) = image_in(j, i, A);

		} // for (i = 0; i < imageToSave.width(); i++)

	} // for (j = 0; j < imageToSave.height(); j++)

	  ////////////////////////////////////////////////////////////////////////////
	imageToSave.write("imageSaved_Gray.png");
	//imageToSave.write(outputFileNamef);

	image_out = imageToSave;

	delete[] sColor_Image.nRed_Arr;
	delete[] sColor_Image.nGreen_Arr;
	delete[] sColor_Image.nBlue_Arr;

	delete[] sGrayscale_Image.nGrayScale_Arr;
	delete[] sGrayscale_Image.nPixel_ValidOrNotArr;

	delete[] sHLS_Image_Before_Processing.fHue_Arr;
	delete[] sHLS_Image_Before_Processing.fLightness_Arr;
	delete[] sHLS_Image_Before_Processing.fSaturation_Arr;

	printf("\n\n fL_Min_Glob = %E, fL_Max_Glob = %E, fA_Min_Glob = %E, fA_Max_Glob = %E, fB_Min_Glob = %E, fB_Max_Glob = %E",
		fL_Min_Glob, fL_Max_Glob, fA_Min_Glob, fA_Max_Glob, fB_Min_Glob, fB_Max_Glob);

	//	printf("\n\n The grayscale image has been saved");
	//printf("\n\n Please press any key to exit");	getchar(); 

	return 1;
	//////////////////////////////////////////////////////////////////////////
} //int doColor_To_Grayscale(...


int Weighted_RGB_FrInitial_RGB_Colors(
	const int nRedf,
	const int nGreenf,
	const int nBluef,

	const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,

	WEIGHTED_RGB_COLORS *sWeighted_RGB_Colorsf) //
{

	sWeighted_RGB_Colorsf->nRed = (int)((float)(nRedf)*sParameters_Color_To_Grayscalef->fWeight_Redf / 100.0);

	if (sWeighted_RGB_Colorsf->nRed > 255)
		sWeighted_RGB_Colorsf->nRed = 255;

	sWeighted_RGB_Colorsf->nGreen = (int)((float)(nGreenf)*sParameters_Color_To_Grayscalef->fWeight_Greenf / 100.0);
	if (sWeighted_RGB_Colorsf->nGreen > 255)
		sWeighted_RGB_Colorsf->nGreen = 255;

	sWeighted_RGB_Colorsf->nBlue = (int)((float)(nBluef)*sParameters_Color_To_Grayscalef->fWeight_Bluef / 100.0);
	if (sWeighted_RGB_Colorsf->nBlue > 255)
		sWeighted_RGB_Colorsf->nBlue = 255;

	return 1;
} //int Weighted_RGB_FrInitial_RGB_Colors(...

int Weighted_CMYK_Fr_RGB_Colors(
	const int nRedf,
	const int nGreenf,
	const int nBluef,

	const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
	WEIGHTED_CMYK_COLORS *sWeighted_CMYK_Colorsf) //
{
	int
		nCyanf,
		nMagentaf,
		nYellowf;

	float
		fCyanf,
		fMagentaf,
		fYellowf,

		fTempf,
		fKf = fLarge; //black

	fCyanf = 1.0 - ((float)(nRedf) / 255.0);

	if (fCyanf < fKf)
		fKf = fCyanf;

	fMagentaf = 1.0 - ((float)(nGreenf) / 255.0);
	if (fMagentaf < fKf)
		fKf = fMagentaf;

	fYellowf = 1.0 - ((float)(nBluef) / 255.0);
	if (fYellowf < fKf)
		fKf = fYellowf;
	//////////////////////////////////////////////////////////
	if (fKf < feps)
		fTempf = 1.0;
	else
		fTempf = 1.0 / (1.0 - fKf);
	////////////////////////////////////////////////////////

	nCyanf = (int)(255.0*(fCyanf - fKf)*fTempf);
	if (nCyanf < 0)
		nCyanf = 0;

	if (nCyanf > 255)
		nCyanf = 255;

	nMagentaf = (int)(255.0*(fMagentaf - fKf)*fTempf);
	if (nMagentaf < 0)
		nMagentaf = 0;

	if (nMagentaf > 255)
		nMagentaf = 255;

	nYellowf = (int)(255.0*(fYellowf - fKf)*fTempf);
	if (nYellowf < 0)
		nYellowf = 0;

	if (nYellowf > 255)
		nYellowf = 255;

	//////////////////////////////////////////////////////////////////////
	sWeighted_CMYK_Colorsf->nCyan = (int)((float)(nCyanf)*sParameters_Color_To_Grayscalef->fWeight_Cyanf / 100.0);
	if (sWeighted_CMYK_Colorsf->nCyan > 255)
		sWeighted_CMYK_Colorsf->nCyan = 255;

	sWeighted_CMYK_Colorsf->nMagenta = (int)((float)(nMagentaf)*sParameters_Color_To_Grayscalef->fWeight_Magentaf / 100.0);
	if (sWeighted_CMYK_Colorsf->nMagenta > 255)
		sWeighted_CMYK_Colorsf->nMagenta = 255;

	sWeighted_CMYK_Colorsf->nYellow = (int)((float)(nYellowf)*sParameters_Color_To_Grayscalef->fWeight_Yellowf / 100.0);
	if (sWeighted_CMYK_Colorsf->nYellow > 255)
		sWeighted_CMYK_Colorsf->nYellow = 255;

	sWeighted_CMYK_Colorsf->nBlack = (int)(255.0*fKf);
	//////////////////////////////////////////////////////////////////////////

	//https://www.rapidtables.com/convert/color/cmyk-to-rgb.html
	sWeighted_CMYK_Colorsf->nRed_FrWeightedCyan = (int)((255 - sWeighted_CMYK_Colorsf->nCyan)*(1.0 - fKf));

	if (sWeighted_CMYK_Colorsf->nRed_FrWeightedCyan > 255)
		sWeighted_CMYK_Colorsf->nRed_FrWeightedCyan = 255;

	sWeighted_CMYK_Colorsf->nGreen_FrWeightedMagenta = (int)((255 - sWeighted_CMYK_Colorsf->nMagenta)*(1.0 - fKf));
	if (sWeighted_CMYK_Colorsf->nGreen_FrWeightedMagenta > 255)
		sWeighted_CMYK_Colorsf->nGreen_FrWeightedMagenta = 255;

	sWeighted_CMYK_Colorsf->nBlue_FrWeightedYellow = (int)((255 - sWeighted_CMYK_Colorsf->nYellow)*(1.0 - fKf));
	if (sWeighted_CMYK_Colorsf->nBlue_FrWeightedYellow > 255)
		sWeighted_CMYK_Colorsf->nBlue_FrWeightedYellow = 255;

	return 1;
} //int Weighted_CMYK_Fr_RGB_Colors(...

  //////////////////////////////////////////////////////////////////////////////////////////
  //D.Sundararajan
  //Digital Image Processing (Springer, 2017)

int XYZ_Fr_RGB_Colors(
	const int nRedf,
	const int nGreenf,
	const int nBluef,

	XYZ_COLORS *sXYZ_Colorsf) //
{

	sXYZ_Colorsf->fX = 0.411*(float)(nRedf)+0.342*(float)(nGreenf)+0.178*(float)(nBluef);

	sXYZ_Colorsf->fY = 0.222*(float)(nRedf)+0.707*(float)(nGreenf)+0.071*(float)(nBluef);

	sXYZ_Colorsf->fZ = 0.020*(float)(nRedf)+0.130*(float)(nGreenf)+0.939*(float)(nBluef);

	return 1;
} //int XYZ_Fr_RGB_Colors(...

int XYZ_To_RGB_Colors(
	const XYZ_COLORS *sXYZ_Colorsf,

	int &nRed_FrXYZf,
	int &nGreen_FrXYZf,
	int &nBlue_FrXYZf)
{

	nRed_FrXYZf = (int)(3.063*sXYZ_Colorsf->fX - 1.393*sXYZ_Colorsf->fY - 0.476*sXYZ_Colorsf->fZ);

	if (nRed_FrXYZf < 0)
		nRed_FrXYZf = 0;

	if (nRed_FrXYZf > 255)
		nRed_FrXYZf = 255;

	nGreen_FrXYZf = (int)(-0.969*sXYZ_Colorsf->fX + 1.876*sXYZ_Colorsf->fY + 0.042*sXYZ_Colorsf->fZ);

	if (nGreen_FrXYZf < 0)
		nGreen_FrXYZf = 0;

	if (nGreen_FrXYZf > 255)
		nGreen_FrXYZf = 255;

	nBlue_FrXYZf = (int)(0.068*sXYZ_Colorsf->fX - 0.229*sXYZ_Colorsf->fY + 1.069*sXYZ_Colorsf->fZ);
	if (nBlue_FrXYZf < 0)
		nBlue_FrXYZf = 0;

	if (nBlue_FrXYZf > 255)
		nBlue_FrXYZf = 255;

	return 1;
} //int XYZ_To_RGB_Colors(...

  ///////////////////////////////////////////////////////////////////
  //XYZ to Lab using the standard illuminant D65 (p.162, D.Sundararajan)
double Funct_LAB_FrXYZ(double dTf)
{
	if (dTf > 0.008856)
		return (cbrt(dTf));
	else
		return (7.787*dTf + (16.0 / 116.0));

} //double Funct_LAB_FrXYZ(double dTf)

  /////////////////////////////////////////////////////////////////////

  //Lab to XYZ using the standard illuminant D65 (p.102, D.Sundararajan)
double Funct_YYZ_FrLAB(double dTf)
{
	double
		dCubef = dTf*dTf*dTf;

	if (dCubef > 0.008856)
		return (dCubef);
	else
		return ((dTf - (16.0 / 116.0)) / 7.787);

} //double Funct_YYZ_FrLAB(double dTf)
  ///////////////////////////////////////////////////////////////////////

int XYZ_To_Weighted_LAB_Colors(
	const XYZ_COLORS *sXYZ_Colorsf,

	const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,

	WEIGHTED_LAB_COLORS *sWeighted_Lab_Colorsf)
{
	double Funct_LAB_FrXYZ(double dTf);

	//Normalized
	double
		dX_Normf = sXYZ_Colorsf->fX / dX_Referf,
		dY_Normf = sXYZ_Colorsf->fY / dY_Referf,
		dZ_Normf = sXYZ_Colorsf->fZ / dZ_Referf;

	double
		dX_Primef = Funct_LAB_FrXYZ(dX_Normf),

		dY_Primef = Funct_LAB_FrXYZ(dY_Normf),
		dZ_Primef = Funct_LAB_FrXYZ(dZ_Normf);

	sWeighted_Lab_Colorsf->fL_FrXYZf = 116.0*dY_Primef - 16.0;

	sWeighted_Lab_Colorsf->fA_FrXYZf = 500.0*(dX_Primef - dY_Primef);

	sWeighted_Lab_Colorsf->fB_FrXYZf = 200.0*(dY_Primef - dZ_Primef);

	if (sWeighted_Lab_Colorsf->fL_FrXYZf < fL_Min_Glob)
		fL_Min_Glob = sWeighted_Lab_Colorsf->fL_FrXYZf;

	if (sWeighted_Lab_Colorsf->fL_FrXYZf > fL_Max_Glob)
		fL_Max_Glob = sWeighted_Lab_Colorsf->fL_FrXYZf;
	/////////////////////////////////////////////////////////////////


	if (sWeighted_Lab_Colorsf->fA_FrXYZf < fA_Min_Glob)
		fA_Min_Glob = sWeighted_Lab_Colorsf->fA_FrXYZf;

	if (sWeighted_Lab_Colorsf->fA_FrXYZf > fA_Max_Glob)
		fA_Max_Glob = sWeighted_Lab_Colorsf->fA_FrXYZf;
	//////////////////////////////////////////////////////////////

	if (sWeighted_Lab_Colorsf->fB_FrXYZf < fB_Min_Glob)
		fB_Min_Glob = sWeighted_Lab_Colorsf->fB_FrXYZf;

	if (sWeighted_Lab_Colorsf->fB_FrXYZf > fB_Max_Glob)
		fB_Max_Glob = sWeighted_Lab_Colorsf->fB_FrXYZf;



	//////////////////////////////////////////////////////////////////

	sWeighted_Lab_Colorsf->fWeighted_L = sParameters_Color_To_Grayscalef->fWeight_Lf*sWeighted_Lab_Colorsf->fL_FrXYZf / 100.0;

	if (sWeighted_Lab_Colorsf->fWeighted_L < 0.0)
		sWeighted_Lab_Colorsf->fWeighted_L = 0.0;

	if (sWeighted_Lab_Colorsf->fWeighted_L > 100.0)
		sWeighted_Lab_Colorsf->fWeighted_L = 100.0;

	sWeighted_Lab_Colorsf->fWeighted_A = sParameters_Color_To_Grayscalef->fWeight_Af*sWeighted_Lab_Colorsf->fA_FrXYZf / 100.0;

	if (sWeighted_Lab_Colorsf->fWeighted_A < -128.0)
		sWeighted_Lab_Colorsf->fWeighted_A = -128.0;

	if (sWeighted_Lab_Colorsf->fWeighted_A > 127.0)
		sWeighted_Lab_Colorsf->fWeighted_A = 127.0;

	sWeighted_Lab_Colorsf->fWeighted_B = sParameters_Color_To_Grayscalef->fWeight_Bf*sWeighted_Lab_Colorsf->fB_FrXYZf / 100.0;

	if (sWeighted_Lab_Colorsf->fWeighted_B < -128.0)
		sWeighted_Lab_Colorsf->fWeighted_B = -128.0;

	if (sWeighted_Lab_Colorsf->fWeighted_B > 127.0)
		sWeighted_Lab_Colorsf->fWeighted_B = 127.0;

	return 1;
} //int XYZ_To_Weighted_LAB_Colors(...

  //////////////////////////////////////////////////////////////////////////////////

int Weighted_LAB_To_XYZ_Colors(
	const WEIGHTED_LAB_COLORS *sWeighted_Lab_Colorsf,

	XYZ_COLORS *sXYZ_Colorsf)
{
	double Funct_YYZ_FrLAB(double dTf);

	double
		dY_Primef = (sWeighted_Lab_Colorsf->fWeighted_L + 16.0) / 116.0;

	sXYZ_Colorsf->fX = dX_Referf*Funct_YYZ_FrLAB((sWeighted_Lab_Colorsf->fWeighted_A / 500.0) + dY_Primef);

	sXYZ_Colorsf->fY = dY_Referf*Funct_YYZ_FrLAB(dY_Primef);

	sXYZ_Colorsf->fZ = dZ_Referf*Funct_YYZ_FrLAB(dY_Primef - (sWeighted_Lab_Colorsf->fWeighted_B / 200.0));


	return 1;
} //int Weighted_LAB_To_XYZ_Colors(...

  ////////////////////////////////////////////////////////////////////////////////

int RGB_Fr_Weighted_LAB_Colors(
	const int nRedf,
	const int nGreenf,
	const int nBluef,

	const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,

	int &nRed_Fr_LABf,
	int &nGreen_Fr_LABf,
	int &nBlue_Fr_LABf)
{

	int XYZ_Fr_RGB_Colors(
		const int nRedf,
		const int nGreenf,
		const int nBluef,

		XYZ_COLORS *sXYZ_Colorsf);

	int XYZ_To_Weighted_LAB_Colors(
		const XYZ_COLORS *sXYZ_Colorsf,

		const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
		WEIGHTED_LAB_COLORS *sWeighted_Lab_Colorsf);

	int Weighted_LAB_To_XYZ_Colors(
		const WEIGHTED_LAB_COLORS *sWeighted_Lab_Colorsf,

		XYZ_COLORS *sXYZ_Colorsf);

	int XYZ_To_RGB_Colors(
		const XYZ_COLORS *sXYZ_Colorsf,

		int &nRed_FrXYZf,
		int &nGreen_FrXYZf,
		int &nBlue_FrXYZf);

	///////////////////////////////////////////////////////
	XYZ_COLORS sXYZ_Colorsf;
	WEIGHTED_LAB_COLORS sWeighted_Lab_Colorsf;
	///////////////////////////////////////////////////
	int
		nResf,
		nRed_FrXYZf,
		nGreen_FrXYZf,
		nBlue_FrXYZf;

	nResf = XYZ_Fr_RGB_Colors(
		nRedf, //const int nRedf,
		nGreenf, //const int nGreenf,
		nBluef, //const int nBluef,

		&sXYZ_Colorsf); // XYZ_COLORS *sXYZ_Colorsf);

	nResf = XYZ_To_Weighted_LAB_Colors(
		&sXYZ_Colorsf, //const XYZ_COLORS *sXYZ_Colorsf,

		sParameters_Color_To_Grayscalef, //const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,

		&sWeighted_Lab_Colorsf); // WEIGHTED_LAB_COLORS *sWeighted_Lab_Colorsf);

	nResf = Weighted_LAB_To_XYZ_Colors(
		&sWeighted_Lab_Colorsf, //const WEIGHTED_LAB_COLORS *sWeighted_Lab_Colorsf,

		&sXYZ_Colorsf); // XYZ_COLORS *sXYZ_Colorsf);

	nResf = XYZ_To_RGB_Colors(
		&sXYZ_Colorsf, //const XYZ_COLORS *sXYZ_Colorsf,

		nRed_FrXYZf, //int &nRed_FrXYZf,
		nGreen_FrXYZf, //int &nGreen_FrXYZf,
		nBlue_FrXYZf); // int &nBlue_FrXYZf);

	nRed_Fr_LABf = nRed_FrXYZf;

	if (nRed_Fr_LABf > 255)
		nRed_Fr_LABf = 255;

	nGreen_Fr_LABf = nGreen_FrXYZf;
	if (nGreen_Fr_LABf > 255)
		nGreen_Fr_LABf = 255;

	nBlue_Fr_LABf = nBlue_FrXYZf;
	if (nBlue_Fr_LABf > 255)
		nBlue_Fr_LABf = 255;

	return 1;
} //int RGB_Fr_Weighted_LAB_Colors(...

  ////////////////////////////////////////////////////////////////////////////////////////////
int Total_OnePixel_RGB_Fr_Weighted_RGB_CMYK_LAB_Colors(
	const int nRedf,
	const int nGreenf,
	const int nBluef,

	const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,

	int &nRed_Totalf,
	int &nGreen_Totalf,
	int &nBlue_Totalf)
{
	int Weighted_RGB_FrInitial_RGB_Colors(
		const int nRedf,
		const int nGreenf,
		const int nBluef,

		const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
		WEIGHTED_RGB_COLORS *sWeighted_RGB_Colorsf);

	int Weighted_CMYK_Fr_RGB_Colors(
		const int nRedf,
		const int nGreenf,
		const int nBluef,

		const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
		WEIGHTED_CMYK_COLORS *sWeighted_CMYK_Colorsf);

	int RGB_Fr_Weighted_LAB_Colors(
		const int nRedf,
		const int nGreenf,
		const int nBluef,

		const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
		int &nRed_Fr_LABf,
		int &nGreen_Fr_LABf,
		int &nBlue_Fr_LABf);
	////////////////////////////////////////////////////

	WEIGHTED_RGB_COLORS sWeighted_RGB_Colorsf;
	WEIGHTED_CMYK_COLORS sWeighted_CMYK_Colorsf;
	///////////////////////////////////////////////////
	int
		nResf,
		nRed_Fr_LABf,
		nGreen_Fr_LABf,
		nBlue_Fr_LABf;
	////////////////////////////////////////////////////////////

	nRed_Totalf = 0;
	nGreen_Totalf = 0;
	nBlue_Totalf = 0;

	if (sParameters_Color_To_Grayscalef->fWeight_Redf < feps && sParameters_Color_To_Grayscalef->fWeight_Greenf < feps && sParameters_Color_To_Grayscalef->fWeight_Bluef < feps)
	{
		sWeighted_RGB_Colorsf.nRed = 0;
		sWeighted_RGB_Colorsf.nGreen = 0;
		sWeighted_RGB_Colorsf.nBlue = 0;

		goto Mark_CMYK;
	} //if (sParameters_Color_To_Grayscalef->fWeight_Redf < feps && sParameters_Color_To_Grayscalef->fWeight_Greenf < feps && sParameters_Color_To_Grayscalef->fWeight_Bluef < feps)

	nResf = Weighted_RGB_FrInitial_RGB_Colors(
		nRedf, //const int nRedf,
		nGreenf, //const int nGreenf,
		nBluef, //const int nBluef,

		sParameters_Color_To_Grayscalef, //const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef)
		&sWeighted_RGB_Colorsf); // WEIGHTED_RGB_COLORS *sWeighted_RGB_Colorsf);

Mark_CMYK: if (sParameters_Color_To_Grayscalef->fWeight_Cyanf < feps && sParameters_Color_To_Grayscalef->fWeight_Magentaf < feps && sParameters_Color_To_Grayscalef->fWeight_Yellowf < feps)
{
	sWeighted_CMYK_Colorsf.nRed_FrWeightedCyan = 0;
	sWeighted_CMYK_Colorsf.nGreen_FrWeightedMagenta = 0;
	sWeighted_CMYK_Colorsf.nBlue_FrWeightedYellow = 0;

	goto Mark_Lab;
} // Mark_CMYK: if (sParameters_Color_To_Grayscalef->fWeight_Cyanf < feps && sParameters_Color_To_Grayscalef->fWeight_Magentaf < feps && sParameters_Color_To_Grayscalef->fWeight_Yellowf < feps)

		   nResf = Weighted_CMYK_Fr_RGB_Colors(
			   nRedf, //const int nRedf,
			   nGreenf, //const int nGreenf,
			   nBluef, //const int nBluef,

			   sParameters_Color_To_Grayscalef, //const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,

			   &sWeighted_CMYK_Colorsf); // WEIGHTED_CMYK_COLORS *sWeighted_CMYK_Colorsf);

	   Mark_Lab: if (sParameters_Color_To_Grayscalef->fWeight_Lf < feps && sParameters_Color_To_Grayscalef->fWeight_Af < feps && sParameters_Color_To_Grayscalef->fWeight_Bf < feps)
	   {
		   nRed_Fr_LABf = 0;
		   nGreen_Fr_LABf = 0;
		   nBlue_Fr_LABf = 0;

		   goto Mark_Total;
	   } //Mark_Lab: if (sParameters_Color_To_Grayscalef->fWeight_Lf < feps && sParameters_Color_To_Grayscalef->fWeight_Af < feps && sParameters_Color_To_Grayscalef->fWeight_Bf < feps)

				 nResf = RGB_Fr_Weighted_LAB_Colors(
					 nRedf, //const int nRedf,
					 nGreenf, //const int nGreenf,
					 nBluef, //const int nBluef,

					 sParameters_Color_To_Grayscalef, //const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
					 nRed_Fr_LABf, //int &nRed_Fr_LABf,
					 nGreen_Fr_LABf, //int &nGreen_Fr_LABf,
					 nBlue_Fr_LABf); // int &nBlue_Fr_LABf);

			 Mark_Total: nRed_Totalf = sWeighted_RGB_Colorsf.nRed + sWeighted_CMYK_Colorsf.nRed_FrWeightedCyan + nRed_Fr_LABf;

				 nGreen_Totalf = sWeighted_RGB_Colorsf.nGreen + sWeighted_CMYK_Colorsf.nGreen_FrWeightedMagenta + nGreen_Fr_LABf;

				 nBlue_Totalf = sWeighted_RGB_Colorsf.nBlue + sWeighted_CMYK_Colorsf.nBlue_FrWeightedYellow + nBlue_Fr_LABf;

				 return 1;
} //int Total_OnePixel_RGB_Fr_Weighted_RGB_CMYK_LAB_Colors(...

  //////////////////////////////////////////////////////////////////////////
int Average_OnePixel_RGB_Fr_Weighted_RGB_CMYK_LAB_Colors(
	const int nRedf,
	const int nGreenf,
	const int nBluef,

	const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
	int &nRed_Averagef,
	int &nGreen_Averagef,
	int &nBlue_Averagef)
{
	int Total_OnePixel_RGB_Fr_Weighted_RGB_CMYK_LAB_Colors(
		const int nRedf,
		const int nGreenf,
		const int nBluef,

		const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,

		int &nRed_Totalf,
		int &nGreen_Totalf,
		int &nBlue_Totalf);

	///////////////////////////////////////////////////
	int
		nResf,

		nNumber_Of_Valid_Color_Spaces = 3, //RGB, CMYK and Lab
		nRed_Totalf,
		nGreen_Totalf,
		nBlue_Totalf;
	////////////////////////////////////////////////////////////
	if (sParameters_Color_To_Grayscalef->fWeight_Redf < 0.0)
	{
		printf("\n\n An error for RGB Red: a weight can not be less than zero");
		fprintf(fout, "\n\n An error for RGB Red: a weight can not be less than zero");

		printf("\n\n Please press any key to exit:");
		fflush(fout); getchar(); exit(1);
	} // if (sParameters_Color_To_Grayscalef->fWeight_Redf < 0.0)

	if (sParameters_Color_To_Grayscalef->fWeight_Greenf < 0.0)
	{
		printf("\n\n An error for RGB Green: a weight can not be less than zero");
		fprintf(fout, "\n\n An error for RGB Green: a weight can not be less than zero");

		printf("\n\n Please press any key to exit:");
		fflush(fout); getchar(); exit(1);
	} //if (sParameters_Color_To_Grayscalef->fWeight_Greenf < 0.0)

	if (sParameters_Color_To_Grayscalef->fWeight_Bluef < 0.0)
	{
		printf("\n\n An error for RGB Blue: a weight can not be less than zero");
		fprintf(fout, "\n\n An error for RGB Blue: a weight can not be less than zero");

		printf("\n\n Please press any key to exit:");
		fflush(fout); getchar(); exit(1);
	} // if (sParameters_Color_To_Grayscalef->fWeight_Bluef < 0.0)

	  //////////////////////////////////////////////////////////////////////////////////////////////////////
	if (sParameters_Color_To_Grayscalef->fWeight_Cyanf < 0.0)
	{
		printf("\n\n An error for Cyan: a weight can not be less than zero");
		fprintf(fout, "\n\n An error for Cyan: a weight can not be less than zero");

		printf("\n\n Please press any key to exit:");
		fflush(fout); getchar(); exit(1);
	} // if (sParameters_Color_To_Grayscalef->fWeight_Cyanf < 0.0)

	if (sParameters_Color_To_Grayscalef->fWeight_Magentaf < 0.0)
	{
		printf("\n\n An error for Magenta: a weight can not be less than zero");
		fprintf(fout, "\n\n An error for Magenta: a weight can not be less than zero");

		printf("\n\n Please press any key to exit:");
		fflush(fout); getchar(); exit(1);
	} // if (sParameters_Color_To_Grayscalef->fWeight_Magentaf < 0.0)

	if (sParameters_Color_To_Grayscalef->fWeight_Yellowf < 0.0)
	{

		printf("\n\n An error for Yellow: a weight can not be less than zero");
		fprintf(fout, "\n\n An error for Yellow: a weight can not be less than zero");

		printf("\n\n Please press any key to exit:");
		fflush(fout); getchar(); exit(1);
	} // if (sParameters_Color_To_Grayscalef->fWeight_Yellowf < 0.0)

	  //////////////////////////////////////////////////////////////////////////////////////////////////////
	if (sParameters_Color_To_Grayscalef->fWeight_Lf < 0.0)
	{

		printf("\n\n An error for Lab L: a weight can not be less than zero");
		fprintf(fout, "\n\n An error for Lab L: a weight can not be less than zero");

		printf("\n\n Please press any key to exit:");
		fflush(fout); getchar(); exit(1);
	} // if (sParameters_Color_To_Grayscalef->fWeight_Lf < 0.0)

	if (sParameters_Color_To_Grayscalef->fWeight_Af < 0.0)
	{
		printf("\n\n An error for Lab A: a weight can not be less than zero");
		fprintf(fout, "\n\n An error for Lab A: a weight can not be less than zero");

		printf("\n\n Please press any key to exit:");
		fflush(fout); getchar(); exit(1);
	} // if (sParameters_Color_To_Grayscalef->fWeight_Af < 0.0)

	if (sParameters_Color_To_Grayscalef->fWeight_Bf < 0.0)
	{

		printf("\n\n An error for Lab B: a weight can not be less than zero");
		fprintf(fout, "\n\n An error for Lab B: a weight can not be less than zero");

		printf("\n\n Please press any key to exit:");
		fflush(fout); getchar(); exit(1);
	} // if (sParameters_Color_To_Grayscalef->fWeight_Bf < 0.0)

	  //////////////////////////////////////////////////////////////////////////////////////////////////
	if (sParameters_Color_To_Grayscalef->fWeight_Redf < feps && sParameters_Color_To_Grayscalef->fWeight_Greenf < feps && sParameters_Color_To_Grayscalef->fWeight_Bluef < feps)
		nNumber_Of_Valid_Color_Spaces -= 1;

	if (sParameters_Color_To_Grayscalef->fWeight_Cyanf < feps && sParameters_Color_To_Grayscalef->fWeight_Magentaf < feps && sParameters_Color_To_Grayscalef->fWeight_Yellowf < feps)
		nNumber_Of_Valid_Color_Spaces -= 1;

	if (sParameters_Color_To_Grayscalef->fWeight_Lf < feps && sParameters_Color_To_Grayscalef->fWeight_Af < feps && sParameters_Color_To_Grayscalef->fWeight_Bf < feps)
		nNumber_Of_Valid_Color_Spaces -= 1;

	if (nNumber_Of_Valid_Color_Spaces <= 0)
	{
		printf("\n\n An error: no color space is selected; all weights are equal to zero ");
		fprintf(fout, "\n\n An error: no color space is selected; all weights are equal to zero ");

		printf("\n\n Please press any key to exit:");
		fflush(fout); getchar(); exit(1);
	} // if (nNumber_Of_Valid_Color_Spaces <= 0)
	  //////////////////////////////////////////////////////////////////////////////////////

	nResf = Total_OnePixel_RGB_Fr_Weighted_RGB_CMYK_LAB_Colors(
		nRedf, //const int nRedf,
		nGreenf, //const int nGreenf,
		nBluef, //const int nBluef,

		sParameters_Color_To_Grayscalef, //const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,

		nRed_Totalf, //int &nRed_Totalf,
		nGreen_Totalf, //int &nGreen_Totalf,
		nBlue_Totalf); // int &nBlue_Totalf);

	nRed_Averagef = (int)((float)(nRed_Totalf) / (float)(nNumber_Of_Valid_Color_Spaces));

	nGreen_Averagef = (int)((float)(nGreen_Totalf) / (float)(nNumber_Of_Valid_Color_Spaces));
	nBlue_Averagef = (int)((float)(nBlue_Totalf) / (float)(nNumber_Of_Valid_Color_Spaces));

	return 1;
} //int Average_OnePixel_RGB_Fr_Weighted_RGB_CMYK_LAB_Colors(...

  //////////////////////////////////////////////////////////////////////
int Converting_AColorImageToAGrayscale(

	const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,

	COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]
	GRAYSCALE_IMAGE *sGrayscale_Imagef) //[nImageSizeMax]
{
	int Initializing_HLS_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		HLS_IMAGE *sHLS_Imagef);

	int Average_OnePixel_RGB_Fr_Weighted_RGB_CMYK_LAB_Colors(
		const int nRedf,
		const int nGreenf,
		const int nBluef,
		const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,

		int &nRed_Averagef,
		int &nGreen_Averagef,
		int &nBlue_Averagef);

	int RGB_Fr_Weighted_HLS(

		const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
		HLS_IMAGE *sHLS_Imagef,

		COLOR_IMAGE *sColor_Imagef);


	int
		nResf,
		nIndexOfPixelCurf,
		iWidf,
		iLenf,
		nRedf,
		nGreenf,
		nBluef,

		nRed_Averagef,
		nGreen_Averagef,
		nBlue_Averagef,

		nGrayIntensityf;

	sGrayscale_Imagef->nWidth = sColor_Imagef->nWidth;
	sGrayscale_Imagef->nLength = sColor_Imagef->nLength;

	if (sParameters_Color_To_Grayscalef->bINCLUDE_RGB_FROM_WEIGHTED_HLS_AFTER_PROCESSINGf == false)
	{
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBluef = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				nResf = Average_OnePixel_RGB_Fr_Weighted_RGB_CMYK_LAB_Colors(
					nRedf, //const int nRedf,
					nGreenf, //const int nGreenf,
					nBluef, //const int nBluef,
					sParameters_Color_To_Grayscalef, //const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,

					nRed_Averagef, //int &nRed_Averagef,
					nGreen_Averagef, //int &nGreen_Averagef,
					nBlue_Averagef); // int &nBlue_Averagef);

				if (nRed_Averagef < 0 || nRed_Averagef > 255 || nGreen_Averagef < 0 || nGreen_Averagef > 255 || nBlue_Averagef < 0 || nBlue_Averagef > 255)
				{
					printf("\n\n An error: wrong values for the averaged colors, the pixel iWidf = %d, iLenf = %d", iWidf, iLenf);
					fprintf(fout, "\n\n An error: wrong values for the averaged colors, the pixel iWidf = %d, iLenf = %d", iWidf, iLenf);

					fprintf(fout, "\n\n nRed_Averagef = %d, nGreen_Averagef = %d, nBlue_Averagef = %d", nRed_Averagef, nGreen_Averagef, nBlue_Averagef);
					printf("\n\n Please press any key to exit:");
					fflush(fout); getchar(); exit(1);
				} // if (nRed_Averagef < 0 || nRed_Averagef > 255 || nGreen_Averagef < 0 || nGreen_Averagef > 255 || nBlue_Averagef < 0 || nBlue_Averagef > 255)

				nGrayIntensityf = (int)(((float)(nRed_Averagef)*sParameters_Color_To_Grayscalef->fWeight_Convert_RedToGrayscalef / 100.0) + ((float)(nGreen_Averagef)*sParameters_Color_To_Grayscalef->fWeight_Convert_GreenToGrayscalef / 100.0) +
					((float)(nBlue_Averagef)*sParameters_Color_To_Grayscalef->fWeight_Convert_BlueToGrayscalef / 100.0));

				sGrayscale_Imagef->nGrayScale_Arr[nIndexOfPixelCurf] = nGrayIntensityf;

				sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1;

			} //for (iLenf = 0; iLenf <  sColor_Imagef->nLength; iLenf++)
		} // for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
	} // if ( sParameters_Color_To_Grayscalef->bINCLUDE_RGB_FROM_WEIGHTED_HLS_AFTER_PROCESSINGf == false)

	HLS_IMAGE
		sHLS_ImageForAveragedColorsf;

	nResf = Initializing_HLS_To_CurSize(
		sGrayscale_Imagef->nWidth, //const int nImageWidthf,
		sGrayscale_Imagef->nLength, //const int nImageLengthf,

		&sHLS_ImageForAveragedColorsf); // COLOR_IMAGE *sColor_Imagef);

	if (sParameters_Color_To_Grayscalef->bINCLUDE_RGB_FROM_WEIGHTED_HLS_AFTER_PROCESSINGf == true)
	{

		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBluef = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				nResf = Average_OnePixel_RGB_Fr_Weighted_RGB_CMYK_LAB_Colors(
					nRedf, //const int nRedf,
					nGreenf, //const int nGreenf,
					nBluef, //const int nBluef,
					sParameters_Color_To_Grayscalef, //const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,

					nRed_Averagef, //int &nRed_Averagef,
					nGreen_Averagef, //int &nGreen_Averagef,
					nBlue_Averagef); // int &nBlue_Averagef);

				if (nRed_Averagef < 0 || nRed_Averagef > 255 || nGreen_Averagef < 0 || nGreen_Averagef > 255 || nBlue_Averagef < 0 || nBlue_Averagef > 255)
				{
					printf("\n\n An error: wrong values for the averaged colors, the pixel iWidf = %d, iLenf = %d", iWidf, iLenf);
					fprintf(fout, "\n\n An error: wrong values for the averaged colors, the pixel iWidf = %d, iLenf = %d", iWidf, iLenf);

					fprintf(fout, "\n\n nRed_Averagef = %d, nGreen_Averagef = %d, nBlue_Averagef = %d", nRed_Averagef, nGreen_Averagef, nBlue_Averagef);
					printf("\n\n Please press any key to exit:");
					fflush(fout); getchar(); exit(1);
				} // if (nRed_Averagef < 0 || nRed_Averagef > 255 || nGreen_Averagef < 0 || nGreen_Averagef > 255 || nBlue_Averagef < 0 || nBlue_Averagef > 255)

				sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nRed_Averagef;
				sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nGreen_Averagef;
				sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nBlue_Averagef;

			} //for (iLenf = 0; iLenf <  sColor_Imagef->nLength; iLenf++)
		} // for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)

		nResf = RGB_Fr_Weighted_HLS(
			sParameters_Color_To_Grayscalef, //const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,

			&sHLS_ImageForAveragedColorsf, //HLS_IMAGE *sHLS_Imagef,

			sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);

							/////////////////////////////////////////////////////////////////////
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBluef = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				nGrayIntensityf = (int)(((float)(nRedf)*sParameters_Color_To_Grayscalef->fWeight_Convert_RedToGrayscalef / 100.0) + ((float)(nGreenf)*sParameters_Color_To_Grayscalef->fWeight_Convert_GreenToGrayscalef / 100.0) +
					((float)(nBluef)*sParameters_Color_To_Grayscalef->fWeight_Convert_BlueToGrayscalef / 100.0));

				sGrayscale_Imagef->nGrayScale_Arr[nIndexOfPixelCurf] = nGrayIntensityf;

				sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1;

			} //for (iLenf = 0; iLenf <  sColor_Imagef->nLength; iLenf++)
		} // for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)

	} //if (sParameters_Color_To_Grayscalef->bINCLUDE_RGB_FROM_WEIGHTED_HLS_AFTER_PROCESSINGf == true)

	delete[] sHLS_ImageForAveragedColorsf.fHue_Arr;
	delete[] sHLS_ImageForAveragedColorsf.fLightness_Arr;
	delete[] sHLS_ImageForAveragedColorsf.fSaturation_Arr;

	return 1;
} //int Converting_AColorImageToAGrayscale(...

int HistogramEqualization(

	GRAYSCALE_IMAGE *sGrayscale_Image)
{
	int
		nNumOfValidPixelsTotf = 0,

		nLookUptableArrf[nNumOfHistogramBinsStat],
		nNumOfValidPixelsForIntensitiesArrf[nNumOfHistogramBinsStat],

		nCumulatNumOfValidPixelsForIntensitiesArrf[nNumOfHistogramBinsStat],

		nWidthImagef = sGrayscale_Image->nWidth,
		nLenImagef = sGrayscale_Image->nLength,

		nIndexOfPixelCurf,

		nIntensityCurf,

		nIntensityMinf = nLarge,
		nIntensityMaxf = -nLarge,

		iIntensityf,
		iWidf,
		iLenf;

	float
		fCumulProbOverIntensitiesArrf[nNumOfHistogramBinsStat];

	for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)
	{
		nNumOfValidPixelsForIntensitiesArrf[iIntensityf] = 0;
		nCumulatNumOfValidPixelsForIntensitiesArrf[iIntensityf] = 0;

		fCumulProbOverIntensitiesArrf[iIntensityf] = 0.0;

		nLookUptableArrf[iIntensityf] = 0;

	} // for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)

	printf("\n\n Equalization: nWidthImagef = %d, nLenImagef = %d", nWidthImagef, nLenImagef);
	fprintf(fout, "\n\n Equalization: nWidthImagef = %d, nLenImagef = %d", nWidthImagef, nLenImagef);

	printf("\n\n Please press any key to continue:");
	fflush(fout); getchar();
	//////////////////////////////////////////////////////////////////////////////
	for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
	{
		for (iLenf = 0; iLenf <nLenImagef; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nLenImagef); // nLenMax);

			if (nIndexOfPixelCurf >= nImageSizeMax)
			{
				printf("\n\n An error: nIndexOfPixelCurf = %d >= nImageSizeMax = %d", nIntensityCurf, nImageSizeMax);

				fprintf(fout, "\n\n An error:  nIndexOfPixelCurf = %d >= nImageSizeMax = %d", nIntensityCurf, nImageSizeMax);
				printf("\n\n Please press any key to exit:");
				fflush(fout); getchar(); exit(1);
			} // if (nIndexOfPixelCurf >= nImageSizeMax)

			if (sGrayscale_Image->nPixel_ValidOrNotArr[nIndexOfPixelCurf] == 0)
				continue;

			nIntensityCurf = sGrayscale_Image->nGrayScale_Arr[nIndexOfPixelCurf];

			if (nIntensityCurf < nIntensityStatMinForEqualization || nIntensityCurf >= nNumOfHistogramBinsStat)
			{
				printf("\n\n An error: the grayscale intensity nIntensityCurf = %d", nIntensityCurf);

				fprintf(fout, "\n\n An error: the grayscale intensity nIntensityCurf = %d", nIntensityCurf);
				printf("\n\n Please press any key to exit:");
				fflush(fout); getchar(); exit(1);
			} //if (nIntensityCurf < nIntensityStatMinForEqualization || nIntensityCurf >= nNumOfHistogramBinsStat)

			if (nIntensityCurf < nIntensityMinf)
				nIntensityMinf = nIntensityCurf;

			if (nIntensityCurf > nIntensityMaxf)
				nIntensityMaxf = nIntensityCurf;

			nNumOfValidPixelsForIntensitiesArrf[nIntensityCurf] += 1;

		} //for (iLenf = 0; iLenf <nLenImagef; iLenf++)
	} //for (iWidf = 0; iWidf < nWidthImagef; iWidf++)


	for (iIntensityf = 1; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)
	{

		nCumulatNumOfValidPixelsForIntensitiesArrf[iIntensityf] += nCumulatNumOfValidPixelsForIntensitiesArrf[iIntensityf - 1] +
			nNumOfValidPixelsForIntensitiesArrf[iIntensityf];

		nNumOfValidPixelsTotf += nNumOfValidPixelsForIntensitiesArrf[iIntensityf];

	} // for (iIntensityf = 1; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)

	printf("\n\n Equalization: nIntensityMinf = %d, nIntensityMaxf = %d, nNumOfValidPixelsTotf = %d", nIntensityMinf, nIntensityMaxf, nNumOfValidPixelsTotf);
	fprintf(fout, "\n\n Equalization: nIntensityMinf = %d, nIntensityMaxf = %d, nNumOfValidPixelsTotf = %d", nIntensityMinf, nIntensityMaxf, nNumOfValidPixelsTotf);

	//printf("\n\n Please press any key to continue:");
	//	fflush(fout); getchar();

	if (nNumOfValidPixelsTotf <= 0)
	{
		printf("\n\n An error: the number of valid pixels nNumOfValidPixelsTotf = %d is wrong", nNumOfValidPixelsTotf);
		fprintf(fout, "\n\n An error: the number  valid pixels nNumOfValidPixelsTotf = %d is wrong", nNumOfValidPixelsTotf);

		printf("\n\n Please press any key to exit:");
		fflush(fout); getchar(); exit(1);
	} //if (nNumOfValidPixelsTotf <= 0)

	for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)
	{
		fCumulProbOverIntensitiesArrf[iIntensityf] = (float)(nCumulatNumOfValidPixelsForIntensitiesArrf[iIntensityf]) / (float)(nNumOfValidPixelsTotf);

		nLookUptableArrf[iIntensityf] = (int)((fCumulProbOverIntensitiesArrf[iIntensityf])*(float)(nIntensityStatMax));

		if (nLookUptableArrf[iIntensityf] > nIntensityStatMax)
		{
			printf("\n\n An error in HistogramEqualization: nLookUptableArrf[%d] = %d > nIntensityStatMax = %d is wrong", iIntensityf, nLookUptableArrf[iIntensityf], nIntensityStatMax);
			fprintf(fout, "\n\n An error in HistogramEqualization: nLookUptableArrf[%d] = %d > nIntensityStatMax = %d is wrong", iIntensityf, nLookUptableArrf[iIntensityf], nIntensityStatMax);

			printf("\n\n Please press any key to exit:");
			fflush(fout); getchar(); exit(1);

		} // if (nLookUptableArrf[iIntensityf] > nIntensityStatMax)

		printf("\n\n Equalization: nLookUptableArrf[%d] = %d", iIntensityf, nLookUptableArrf[iIntensityf]);
		fprintf(fout, "\n\n Equalization: nLookUptableArrf[%d] = %d", iIntensityf, nLookUptableArrf[iIntensityf]);

	} // for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)

	  //printf("\n\n Please press any key to continue:");
	  //	fflush(fout); getchar();
	  ///////////////////////////////////////////////////////////////////////////////////////////////////
	  //printf( "\n\n nLookUptableArrf[0] = %d, nLookUptableArrf[51] = %d", nLookUptableArrf[0], nLookUptableArrf[51]);
	  //fprintf(fout, "\n\n nLookUptableArrf[0] = %d, nLookUptableArrf[51] = %d", nLookUptableArrf[0], nLookUptableArrf[51]);


	for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
	{
		for (iLenf = 0; iLenf < nLenImagef; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nLenImagef); // nLenMax);

			if (nIndexOfPixelCurf >= nImageSizeMax)
			{
				printf("\n\n An error 2: nIndexOfPixelCurf = %d >= nImageSizeMax = %d", nIntensityCurf, nImageSizeMax);

				fprintf(fout, "\n\n An error 2:  nIndexOfPixelCurf = %d >= nImageSizeMax = %d", nIntensityCurf, nImageSizeMax);
				printf("\n\n Please press any key to exit:");
				fflush(fout); getchar(); exit(1);
			} // if (nIndexOfPixelCurf >= nImageSizeMax)

			if (sGrayscale_Image->nPixel_ValidOrNotArr[nIndexOfPixelCurf] == 0)
				continue;

			nIntensityCurf = sGrayscale_Image->nGrayScale_Arr[nIndexOfPixelCurf];

			sGrayscale_Image->nGrayScale_Arr[nIndexOfPixelCurf] = nLookUptableArrf[nIntensityCurf];

			fprintf(fout, "\n\n iWidf = %d, iLenf = %d, sGrayscale_Image->nGrayScale_Arr[%d] = %d",
				iWidf, iLenf, nIndexOfPixelCurf, sGrayscale_Image->nGrayScale_Arr[nIndexOfPixelCurf]);

			fprintf(fout, "\n nIntensityCurf = %d, nLookUptableArrf[%d] = %d", nIntensityCurf, nIntensityCurf, nLookUptableArrf[nIntensityCurf]);

		} //for (iLenf = 0; iLenf <nLenImagef; iLenf++)
	} //for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

	printf("\n\n The end of 'HistogramEqualization': please press any key to continue");
	fflush(fout); getchar();

	return 1;
} //int HistogramEqualization(....

  ///////////////////////////////////////////////////////////////////////////

  //From Wikipedia, https://en.wikipedia.org/wiki/HSL_and_HSV
  //From I.Pitas, Digital Image Processing Algorithms and Applications, 2000, p.33
  // Alternative: Practical algorithms for image analysis, Michael Seul and others, p.53

int HLS_Fr_RGB(
	const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

	HLS_IMAGE *sHLS_Imagef)
{
	int
		nImageLengthf = sColor_Imagef->nLength,
		nImageWidthf = sColor_Imagef->nWidth,

		//nIndexCurSizef, // = iLenf + (iWidf*nImageLengthf);
		nIndexOfPixelCurf,
		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nColorMinf,
		nColorMaxf,

		iLenf,
		iWidf;

	float
		fCoefOfPixelIntensNormalizationf = 1.0 / (float)(nIntensityStatMax),

		fHueNotNormalisedArrf[nImageSizeMax],

		fPixelIntensitiesNormalizedArrf[nImageSizeMax],

		fOnePixelIntensNormalizedMinf,
		fOnePixelIntensNormalizedMaxf,

		fOnePixelIntensNormalizedRangef,

		fUNDEFINEDF = fHueUndefinedValue, //-1.0,

		fDiffCurf,

		fLightnessCurf,
		fSaturationCurf,
		fHueCurf,

		fRedNormCurf,
		fGreenNormCurf,
		fBlueNormCurf;

	/////////////////////////////////////////////////////////////////////////////////////////////

	sHLS_Imagef->nLength = nImageLengthf;
	sHLS_Imagef->nWidth = nImageWidthf;

	//printf("\n\n An error in 'HLS_Fr_RGB': please choose only one way to calulate Hue");

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		//for (iLenf = 8; iLenf < nImageLengthf; iLenf++)
		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			//nIndexOfPixelCurf = iLenf + (iWidf*nLenMax);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			fRedNormCurf = (float)(nRedCurf)*fCoefOfPixelIntensNormalizationf;

			fGreenNormCurf = (float)(nGreenCurf)*fCoefOfPixelIntensNormalizationf;
			fBlueNormCurf = (float)(nBlueCurf)*fCoefOfPixelIntensNormalizationf;

			//fprintf(fout, "\n\n 'HLS_Fr_RGB': nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d", nRedCurf, nGreenCurf, nBlueCurf);
			//fprintf(fout, "\n iWidf = %d, iLenf = %d, fRedNormCurf = %E, fGreenNormCurf = %E, fBlueNormCurf = %E", iWidf, iLenf, fRedNormCurf, fGreenNormCurf, fBlueNormCurf);

			//min
			if (nRedCurf <= nGreenCurf)
			{
				if (nRedCurf <= nBlueCurf)
				{
					nColorMinf = nRedCurf;
					fOnePixelIntensNormalizedMinf = fRedNormCurf;
				} // if (nRedCurf <= nBlueCurf)
				else
				{
					nColorMinf = nBlueCurf;
					fOnePixelIntensNormalizedMinf = fBlueNormCurf;

				} //else
			} // if (nRedCurf <= nGreenCurf)
			else if (nRedCurf > nGreenCurf)
			{
				if (nGreenCurf <= nBlueCurf)
				{
					nColorMinf = nGreenCurf;
					fOnePixelIntensNormalizedMinf = fGreenNormCurf;
				} // if (nGreenCurf <= nBlueCurf)
				else
				{
					nColorMinf = nBlueCurf;
					fOnePixelIntensNormalizedMinf = fBlueNormCurf;
				} // else

			} // else if (nRedCurf > nGreenCurf)


			  //Max
			if (nRedCurf >= nGreenCurf)
			{
				if (nRedCurf >= nBlueCurf)
				{
					nColorMaxf = nRedCurf;
					fOnePixelIntensNormalizedMaxf = fRedNormCurf;
				} // if (nRedCurf >= nBlueCurf)
				else
				{
					nColorMaxf = nBlueCurf;
					fOnePixelIntensNormalizedMaxf = fBlueNormCurf;

				} //else
			} // if (nRedCurf >= nGreenCurf)
			else if (nRedCurf < nGreenCurf)
			{
				if (nGreenCurf >= nBlueCurf)
				{
					nColorMaxf = nGreenCurf;
					fOnePixelIntensNormalizedMaxf = fGreenNormCurf;
				} // if (nGreenCurf >= nBlueCurf)
				else
				{
					nColorMaxf = nBlueCurf;
					fOnePixelIntensNormalizedMaxf = fBlueNormCurf;
				} // else

			} // else if (nRedCurf < nGreenCurf)

			  /////////////////////////////////////////////////////////////////////////////////////////////
			fLightnessCurf = (fOnePixelIntensNormalizedMinf + fOnePixelIntensNormalizedMaxf) / 2.0;

			if (nColorMinf == nColorMaxf)
			{
				fSaturationCurf = 0.0;
				fHueCurf = fUNDEFINEDF;
			} // if (nColorMinf == nColorMaxf)
			else
			{

				fOnePixelIntensNormalizedRangef = fOnePixelIntensNormalizedMaxf - fOnePixelIntensNormalizedMinf;

				//if (fOnePixelIntensNormalizedRangef <= 0.0 || fOnePixelIntensNormalizedRangef > fLarge)
				if (fOnePixelIntensNormalizedRangef <= -feps || fOnePixelIntensNormalizedRangef > fLarge)
				{
					//fOnePixelIntensNormalizedRangef = 0.0;

					printf("\n\n An error in 'HLS_Fr_RGB': fOnePixelIntensNormalizedRangef = %E <= 0.0 || ...", fOnePixelIntensNormalizedRangef);
					fprintf(fout, "\n\n An error in 'HLS_Fr_RGB': fOnePixelIntensNormalizedRangef = %E <= 0.0 || ...", fOnePixelIntensNormalizedRangef);

					printf("\n\n Please press any key to exit");
					fflush(fout); getchar(); exit(1);

				} //if (fOnePixelIntensNormalizedRangef <= -feps || fOnePixelIntensNormalizedRangef > fLarge)

				  //saturation
				if (fLightnessCurf < 0.5)
				{
					fSaturationCurf = fOnePixelIntensNormalizedRangef / (fOnePixelIntensNormalizedMinf + fOnePixelIntensNormalizedMaxf);
				} // if (fLightnessCurf < 0.5)
				else // fLightnessCurf >= 0.5
				{
					fDiffCurf = 2.0 - fOnePixelIntensNormalizedMinf - fOnePixelIntensNormalizedMaxf;

					if (fDiffCurf <= 0.0 || fDiffCurf > fLarge)
					{
						fDiffCurf = 0.0;

						printf("\n\n An error in 'HLS_Fr_RGB': fDiffCurf = %E <= 0.0 || ...", fDiffCurf);
						fprintf(fout, "\n\n An error in 'HLS_Fr_RGB': fDiffCurf = %E <= 0.0 || ...", fDiffCurf);

						printf("\n\n Please press any key to exit");
						fflush(fout); getchar(); exit(1);


					} //if (fDiffCurf <= 0.0 || fDiffCurf > fLarge)

					fSaturationCurf = fOnePixelIntensNormalizedRangef / fDiffCurf;
				}// else // if (fLightnessCurf >= 0.5)

				 //hue
				if (fRedNormCurf == fOnePixelIntensNormalizedMaxf)
				{
					//fHueCurf = (fGreenNormCurf - fBlueNormCurf) / fOnePixelIntensNormalizedRangef;
					fHueCurf = fmod((fGreenNormCurf - fBlueNormCurf) / fOnePixelIntensNormalizedRangef, 6.0);

				}// if (fRedNormCurf == fOnePixelIntensNormalizedMaxf)
				else // if (fRedNormCurf == fOnePixelIntensNormalizedMaxf)
				{

					if (fGreenNormCurf == fOnePixelIntensNormalizedMaxf)
					{
						fHueCurf = 2.0 + (fBlueNormCurf - fRedNormCurf) / fOnePixelIntensNormalizedRangef;

					}// if (fGreenNormCurf == fOnePixelIntensNormalizedMaxf)
					else
					{
						if (fBlueNormCurf == fOnePixelIntensNormalizedMaxf)
						{
							fHueCurf = 4.0 + (fRedNormCurf - fGreenNormCurf) / fOnePixelIntensNormalizedRangef;

						}// if (fBlueNormCurf == fOnePixelIntensNormalizedMaxf)
						else
						{

							printf("\n\n An error in 'HLS_Fr_RGB': a mismatch between fOnePixelIntensNormalizedMaxf = %E and the colors", fOnePixelIntensNormalizedMaxf);
							printf("\n fRedNormCurf = %E, fGreenNormCurf = %E, fBlueNormCurf = %E", fRedNormCurf, fGreenNormCurf, fBlueNormCurf);

							fprintf(fout, "\n\n An error in 'HLS_Fr_RGB':  a mismatch between fOnePixelIntensNormalizedMaxf = %E and the colors", fOnePixelIntensNormalizedMaxf);
							fprintf(fout, "\n fRedNormCurf = %E, fGreenNormCurf = %E, fBlueNormCurf = %E", fRedNormCurf, fGreenNormCurf, fBlueNormCurf);

							fprintf(fout, "\n iWidf = %d, nImageWidthf = %d, iLenf = %d, nImageLengthf = %d", iWidf, nImageWidthf, iLenf, nImageLengthf);

							printf("\n\n Please press any key to exit");
							fflush(fout); getchar(); exit(1);

						}// else

					} // else if (fGreenNormCurf != fOnePixelIntensNormalizedMaxf)

				} // else if (fRedNormCurf != fOnePixelIntensNormalizedMaxf)

				fHueCurf = fHueCurf*60.0;

				if (fHueCurf < 0.0)
					fHueCurf = fHueCurf + 360.0;

			}// else if (nColorMinf != nColorMaxf)

			sHLS_Imagef->fHue_Arr[nIndexOfPixelCurf] = fHueCurf;
			sHLS_Imagef->fLightness_Arr[nIndexOfPixelCurf] = fLightnessCurf;
			sHLS_Imagef->fSaturation_Arr[nIndexOfPixelCurf] = fSaturationCurf;

			if ((iWidf / 50) * 50 == iWidf && (iLenf / 50) * 50 == iLenf)
			{

				fprintf(fout, "\n\n 'HLS_Fr_RGB': iWidf = %d, iLenf = %d", iWidf, iLenf);
				fprintf(fout, "\n fHueCurf = %E, fLightnessCurf = %E, fSaturationCurf = %E", fHueCurf, fLightnessCurf, fSaturationCurf);
			} // if ((iWidf / 50) * 50 == iWidf && (iLenf / 50) * 50 == iLenf)

		} // for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} //for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return 1;
} // int HLS_Fr_RGB(...

  ///////////////////////////////////////////////////////////////////////////
  //I.Pitas, pp.36-37
int RGB_Fr_HLS(

	const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
	const HLS_IMAGE *sHLS_Imagef,

	COLOR_IMAGE *sColor_Imagef)
{
	float RGB_Intermediate(
		float &fN1f,
		float &fN2f,
		float &fHuef);

	int
		nImageLengthf = sHLS_Imagef->nLength,
		nImageWidthf = sHLS_Imagef->nWidth,

		//nIndexCurSizef, // = iLenf + (iWidf*nImageLengthf);
		nIndexOfPixelCurf,
		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		iLenf,
		iWidf;

	float
		fUNDEFINEDF = fHueUndefinedValue, //-1.0,

		fDiffCurf,

		fLightnessCurf,
		fSaturationCurf,
		fHueCurf,

		fHueTempf,

		fM1f,
		fM2f,

		fRedNormCurf,
		fGreenNormCurf,
		fBlueNormCurf;

	/////////////////////////////////////////////////////////////////////////////////////////////

	sColor_Imagef->nLength = nImageLengthf;
	sColor_Imagef->nWidth = nImageWidthf;

	//printf("\n\n An error in 'RGB_Fr_HLS': please choose only one way to calulate Hue");

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		//for (iLenf = 8; iLenf < nImageLengthf; iLenf++)
		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			//nIndexOfPixelCurf = iLenf + (iWidf*nLenMax);

			fHueCurf = sHLS_Imagef->fHue_Arr[nIndexOfPixelCurf];

			fLightnessCurf = sHLS_Imagef->fLightness_Arr[nIndexOfPixelCurf];
			fSaturationCurf = sHLS_Imagef->fSaturation_Arr[nIndexOfPixelCurf];

			if (fLightnessCurf <= 0.5)
			{
				fM2f = fLightnessCurf*(1.0 + fSaturationCurf);
			} //if (fLightnessCurf <= 0.5)
			else
			{
				fM2f = fLightnessCurf + fSaturationCurf - (fLightnessCurf*fSaturationCurf);
			}//else

			fM1f = (2.0*fLightnessCurf) - fM2f;

			if (fSaturationCurf == 0.0)
			{
				if (fHueCurf == fUNDEFINEDF)
				{
					fRedNormCurf = fLightnessCurf;
					fGreenNormCurf = fLightnessCurf;
					fBlueNormCurf = fLightnessCurf;

				} // if (fHueCurf == fUNDEFINEDF)
				else // if (fHueCurf != fUNDEFINEDF)
				{
					fRedNormCurf = 0.0;
					fGreenNormCurf = 0.0;
					fBlueNormCurf = 0.0;

					printf("\n\n A possible error in 'RGB_Fr_HLS':  fHueCurf = %E at if (fSaturationCurf == 0.0)", fHueCurf);

					printf("\n iWidf = %d, nImageWidthf = %d, iLenf = %d, nImageLengthf = %d", iWidf, nImageWidthf, iLenf, nImageLengthf);


					fprintf(fout, "\n\n A possible error in 'RGB_Fr_HLS':  fHueCurf = %E at if (fSaturationCurf == 0.0)", fHueCurf);

					fprintf(fout, "\n iWidf = %d, nImageWidthf = %d, iLenf = %d, nImageLengthf = %d", iWidf, nImageWidthf, iLenf, nImageLengthf);

					printf("\n\n Please press any key to exit");
					fflush(fout); getchar(); exit(1);

				} // else // if (fHueCurf != fUNDEFINEDF)

			}//if (fSaturationCurf == 0.0)
			else //if (fSaturationCurf != 0.0)
			{

				if (fHueCurf < sParameters_Color_To_Grayscalef->nHueMinf || fHueCurf > sParameters_Color_To_Grayscalef->nHueMaxf)
				{
					nRedCurf = nIntensityStatMax;
					nGreenCurf = nIntensityStatMax;
					nBlueCurf = nIntensityStatMax;

					goto MarkColorAssignment;
				} // if (fHueCurf < sParameters_Color_To_Grayscalef->nHueMinf || fHueCurf > sParameters_Color_To_Grayscalef->nHueMaxf)

				fHueTempf = fHueCurf + 120.0;
				fRedNormCurf = RGB_Intermediate(
					fM1f, //float &fN1f,
					fM2f, //float &fN2f,
					fHueTempf); // float &fHuef);

				fHueTempf = fHueCurf; //?
				fGreenNormCurf = RGB_Intermediate(
					fM1f, //float &fN1f,
					fM2f, //float &fN2f,
					fHueCurf); // float &fHuef); //changed fHueCurf?


				fHueTempf = fHueCurf - 120.0;
				fBlueNormCurf = RGB_Intermediate(
					fM1f, //float &fN1f,
					fM2f, //float &fN2f,
					fHueTempf); // float &fHuef);

			} //if (fSaturationCurf != 0.0)

			nRedCurf = (int)(fRedNormCurf*(float)(nIntensityStatMax));

			nGreenCurf = (int)(fGreenNormCurf*(float)(nIntensityStatMax));
			nBlueCurf = (int)(fBlueNormCurf*(float)(nIntensityStatMax));

			///////////////////////////////////////////
			if (nRedCurf > nIntensityStatMax)
				nRedCurf = nIntensityStatMax;

			if (nRedCurf < 0)
				nRedCurf = 0;

			//////////////////////////////////////////////////
			if (nGreenCurf > nIntensityStatMax)
				nGreenCurf = nIntensityStatMax;

			if (nGreenCurf < 0)
				nGreenCurf = 0;


			////////////////////////////////////////////////////////
			if (nBlueCurf > nIntensityStatMax)
				nBlueCurf = nIntensityStatMax;

			if (nBlueCurf < 0)
				nBlueCurf = 0;

			if (sParameters_Color_To_Grayscalef->bSELECTING_COLOR_RANGES_AT_THE_HLS_OUTPUTf == true)
			{
				if (nRedCurf < sParameters_Color_To_Grayscalef->nRedMinf || nRedCurf > sParameters_Color_To_Grayscalef->nRedMaxf)
				{
					nRedCurf = nIntensityStatMax;
					nGreenCurf = nIntensityStatMax;
					nBlueCurf = nIntensityStatMax;

					goto MarkColorAssignment;
				} //if (nRedCurf < sParameters_Color_To_Grayscalef->nRedMinf || nRedCurf > sParameters_Color_To_Grayscalef->nRedMaxf)

				if (nGreenCurf < sParameters_Color_To_Grayscalef->nGreenMinf || nGreenCurf > sParameters_Color_To_Grayscalef->nGreenMaxf)
				{
					nGreenCurf = nIntensityStatMax;
					nGreenCurf = nIntensityStatMax;
					nBlueCurf = nIntensityStatMax;

					goto MarkColorAssignment;
				} //if (nGreenCurf < sParameters_Color_To_Grayscalef->nGreenMinf || nGreenCurf > sParameters_Color_To_Grayscalef->nGreenMaxf)

				if (nBlueCurf < sParameters_Color_To_Grayscalef->nBlueMinf || nBlueCurf > sParameters_Color_To_Grayscalef->nBlueMaxf)
				{
					nBlueCurf = nIntensityStatMax;
					nGreenCurf = nIntensityStatMax;
					nBlueCurf = nIntensityStatMax;

					goto MarkColorAssignment;
				} //if (nBlueCurf < sParameters_Color_To_Grayscalef->nBlueMinf || nBlueCurf > sParameters_Color_To_Grayscalef->nBlueMaxf)

			} // if (sParameters_Color_To_Grayscalef->bSELECTING_COLOR_RANGES_AT_THE_HLS_OUTPUTf == true)
			  ///////////////////////////////////////////////////////////////////
		MarkColorAssignment: sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nRedCurf;
			sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nGreenCurf;
			sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nBlueCurf;

		} // for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} //for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return 1;
} // int RGB_Fr_HLS(...

float RGB_Intermediate(
	float &fN1f,
	float &fN2f,
	float &fHuef)
{
	if (fHuef > 360.0)
		fHuef -= 360.0;

	if (fHuef < 0.0)
		fHuef += 360.0;

	if (fHuef < 60.0)
	{
		return (fN1f + ((fN2f - fN1f)*fHuef / 60.0));
	}//if (fHuef < 60.0)

	if (fHuef < 180.0)
	{
		return (fN2f);
	}//if (fHuef < 180.0)

	if (fHuef < 240.0)
	{
		return (fN1f + ((fN2f - fN1f)*(240.0 - fHuef) / 60.0));
	}//if (fHuef < 240.0)

	return (fN1f);

}//float RGB_Intermediate(...

 /////////////////////////////////////////////////////////////////////////////
int Weighted_HLS_Colors_One_Pixel(
	const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
	float &fHuef,
	float &fLightnessf,
	float &fSaturationf)
{

	fHuef = fHuef*sParameters_Color_To_Grayscalef->fWeightOfHuef / 100.0;
	if (fHuef > 360.0)
		fHuef = 360.0;

	fLightnessf = fLightnessf*sParameters_Color_To_Grayscalef->fWeightOfLightnessf / 100.0;
	if (fLightnessf > 1.0)
		fLightnessf = 1.0;


	fSaturationf = fSaturationf*sParameters_Color_To_Grayscalef->fWeightOfSaturationf / 100.0;
	if (fSaturationf > 1.0)
		fSaturationf = 1.0;

	return 1;
} //int Weighted_HLS_Colors_One_Pixel(...
  ///////////////////////////////////////////////////////////////////

int Weighting_HLS_Image(
	const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
	HLS_IMAGE *sHLS_Imagef)
{

	int Weighted_HLS_Colors_One_Pixel(
		const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
		float &fHuef,
		float &fLightnessf,
		float &fSaturationf);

	int
		nResf,
		nImageLengthf = sHLS_Imagef->nLength,
		nImageWidthf = sHLS_Imagef->nWidth,

		//nIndexCurSizef, // = iLenf + (iWidf*nImageLengthf);
		nIndexOfPixelCurf,

		iLenf,
		iWidf;

	float
		fUNDEFINEDF = -1.0,

		fDiffCurf,

		fLightnessCurf,
		fSaturationCurf,
		fHueCurf;

	/////////////////////////////////////////////////////////////////////////////////////////////

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		//for (iLenf = 8; iLenf < nImageLengthf; iLenf++)
		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			fHueCurf = sHLS_Imagef->fHue_Arr[nIndexOfPixelCurf];
			fLightnessCurf = sHLS_Imagef->fLightness_Arr[nIndexOfPixelCurf];
			fSaturationCurf = sHLS_Imagef->fSaturation_Arr[nIndexOfPixelCurf];

			if ((iWidf / 50) * 50 == iWidf && (iLenf / 50) * 50 == iLenf)
			{

				fprintf(fout, "\n\n 'Weighting_HLS_Image': iWidf = %d, iLenf = %d", iWidf, iLenf);
				fprintf(fout, "\n fHueCurf = %E, fLightnessCurf = %E, fSaturationCurf = %E", fHueCurf, fLightnessCurf, fSaturationCurf);
			} // if ((iWidf / 50) * 50 == iWidf && (iLenf / 50) * 50 == iLenf)

			nResf = Weighted_HLS_Colors_One_Pixel(

				sParameters_Color_To_Grayscalef,
				fHueCurf, //float &fHuef,
				fLightnessCurf, //float &fLightnessf,
				fSaturationCurf); // float &fSaturationf);

			sHLS_Imagef->fHue_Arr[nIndexOfPixelCurf] = fHueCurf;
			sHLS_Imagef->fLightness_Arr[nIndexOfPixelCurf] = fLightnessCurf;
			sHLS_Imagef->fSaturation_Arr[nIndexOfPixelCurf] = fSaturationCurf;

		} // for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} //for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return 1;
} //int Weighting_HLS_Image(


int RGB_Fr_Weighted_HLS(

	const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
	HLS_IMAGE *sHLS_Imagef,

	COLOR_IMAGE *sColor_Imagef)
{
	int HLS_Fr_RGB(
		const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

		HLS_IMAGE *sHLS_Imagef);

	int Weighting_HLS_Image(
		const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
		HLS_IMAGE *sHLS_Imagef);

	int RGB_Fr_HLS(
		const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
		const HLS_IMAGE *sHLS_Imagef,

		COLOR_IMAGE *sColor_Imagef);
	///////////////////////////////////////////////////
	int
		nResf;

	nResf = HLS_Fr_RGB(
		sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

		sHLS_Imagef); // HLS_IMAGE *sHLS_Imagef);

	nResf = Weighting_HLS_Image(
		sParameters_Color_To_Grayscalef, //const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
		sHLS_Imagef); // HLS_IMAGE *sHLS_Imagef);

	nResf = RGB_Fr_HLS(
		sParameters_Color_To_Grayscalef, //const PARAMETERS_COLOR_TO_GRAYSCALE *sParameters_Color_To_Grayscalef,
		sHLS_Imagef, //const HLS_IMAGE *sHLS_Imagef,

		sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);

	return 1;
} // int RGB_Fr_Weighted_HLS(...

int Initializing_Color_To_CurSize(
	const int nImageWidthf,
	const int nImageLengthf,

	COLOR_IMAGE *sColor_Imagef) //[]
{
	int
		nIndexOfPixelCurf,

		nImageSizeCurf = nImageWidthf*nImageLengthf,
		iWidf,
		iLenf;

	sColor_Imagef->nWidth = nImageWidthf;
	sColor_Imagef->nLength = nImageLengthf;

	//sGrayscale_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];
	sColor_Imagef->nRed_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nGreen_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nBlue_Arr = new int[nImageSizeCurf];

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			//sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1; //valid for no restrictions
			sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = -1;

		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return 1;
} //int Initializing_Color_To_CurSize(...

int Initializing_HLS_To_CurSize(
	const int nImageWidthf,
	const int nImageLengthf,

	HLS_IMAGE *sHLS_Imagef) //[]
{
	int
		nIndexOfPixelCurf,

		nImageSizeCurf = nImageWidthf*nImageLengthf,
		iWidf,
		iLenf;

	sHLS_Imagef->nWidth = nImageWidthf;
	sHLS_Imagef->nLength = nImageLengthf;

	//sGrayscale_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];
	sHLS_Imagef->fHue_Arr = new float[nImageSizeCurf];
	sHLS_Imagef->fLightness_Arr = new float[nImageSizeCurf];
	sHLS_Imagef->fSaturation_Arr = new float[nImageSizeCurf];

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			//sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1; //valid for no restrictions
			sHLS_Imagef->fHue_Arr[nIndexOfPixelCurf] = -1.0;
			sHLS_Imagef->fLightness_Arr[nIndexOfPixelCurf] = -1.0;
			sHLS_Imagef->fSaturation_Arr[nIndexOfPixelCurf] = -1.0;

		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return 1;
} //int Initializing_HLS_To_CurSize(...

int Initializing_GRAYSCALE_To_CurSize(
	const int nImageWidthf,
	const int nImageLengthf,

	GRAYSCALE_IMAGE *sGRAYSCALE_Imagef) //[]
{
	int
		nIndexOfPixelCurf,

		nImageSizeCurf = nImageWidthf*nImageLengthf,
		iWidf,
		iLenf;

	sGRAYSCALE_Imagef->nWidth = nImageWidthf;
	sGRAYSCALE_Imagef->nLength = nImageLengthf;

	//sGrayscale_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];
	sGRAYSCALE_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];
	sGRAYSCALE_Imagef->nGrayScale_Arr = new int[nImageSizeCurf];

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			sGRAYSCALE_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1; //valid for all pixels
			sGRAYSCALE_Imagef->nGrayScale_Arr[nIndexOfPixelCurf] = -1;

		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return 1;
} //int Initializing_GRAYSCALE_To_CurSize(...

  //printf("\n\n Please press any key:"); getchar();
